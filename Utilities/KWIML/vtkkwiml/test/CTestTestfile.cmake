# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Utilities/KWIML/vtkkwiml/test
# Build directory: /usr/src/Paraview5/VTK/Utilities/KWIML/vtkkwiml/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkkwiml.test "/usr/src/Paraview5/VTK/Utilities/KWIML/vtkkwiml/test/kwiml_test")
