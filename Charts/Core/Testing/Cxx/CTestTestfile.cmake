# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Charts/Core/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Charts/Core/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkChartsCoreCxx-TestContextScene "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextScene" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkChartsCoreCxx-TestContextScene PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestControlPointsItem "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestControlPointsItem" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkChartsCoreCxx-TestControlPointsItem PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestControlPointsItemEvents "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestControlPointsItemEvents" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkChartsCoreCxx-TestControlPointsItemEvents PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestAreaPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestAreaPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestAreaPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestAreaPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestAxes "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestAxes" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestAxes.png")
set_tests_properties(vtkChartsCoreCxx-TestAxes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestBagPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestBagPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestBagPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestBagPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestBarGraph "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestBarGraph" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestBarGraph.png")
set_tests_properties(vtkChartsCoreCxx-TestBarGraph PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestBarGraphHorizontal "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestBarGraphHorizontal" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestBarGraphHorizontal.png")
set_tests_properties(vtkChartsCoreCxx-TestBarGraphHorizontal PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestBarGraphSelection "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestBarGraphSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestBarGraphSelection.png")
set_tests_properties(vtkChartsCoreCxx-TestBarGraphSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestBoxPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestBoxPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestBoxPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestBoxPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestCategoryLegend "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestCategoryLegend" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestCategoryLegend.png")
set_tests_properties(vtkChartsCoreCxx-TestCategoryLegend PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestColorTransferFunction "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestColorTransferFunction" "-E" "80" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestColorTransferFunction.png")
set_tests_properties(vtkChartsCoreCxx-TestColorTransferFunction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartDouble "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartDouble" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartDouble.png")
set_tests_properties(vtkChartsCoreCxx-TestChartDouble PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartDoubleColors "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartDoubleColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartDoubleColors.png")
set_tests_properties(vtkChartsCoreCxx-TestChartDoubleColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartMatrix "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartMatrix" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartMatrix.png")
set_tests_properties(vtkChartsCoreCxx-TestChartMatrix PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartTileScaling "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartTileScaling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartTileScaling.png")
set_tests_properties(vtkChartsCoreCxx-TestChartTileScaling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartUnicode "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartUnicode" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Fonts/DejaVuSans.ttf" "-E" "25" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartUnicode.png")
set_tests_properties(vtkChartsCoreCxx-TestChartUnicode PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartsOn3D "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartsOn3D" "-E" "16" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartsOn3D.png")
set_tests_properties(vtkChartsCoreCxx-TestChartsOn3D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartXYInvertedAxis "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartXYInvertedAxis" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartXYInvertedAxis.png")
set_tests_properties(vtkChartsCoreCxx-TestChartXYInvertedAxis PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestChartXYZ "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestChartXYZ" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestChartXYZ.png")
set_tests_properties(vtkChartsCoreCxx-TestChartXYZ PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContext "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContext" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContext.png")
set_tests_properties(vtkChartsCoreCxx-TestContext PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextArea "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextArea" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextArea.png")
set_tests_properties(vtkChartsCoreCxx-TestContextArea PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextAreaFixedAspect "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextAreaFixedAspect" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextAreaFixedAspect.png")
set_tests_properties(vtkChartsCoreCxx-TestContextAreaFixedAspect PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextAreaFixedMargins "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextAreaFixedMargins" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextAreaFixedMargins.png")
set_tests_properties(vtkChartsCoreCxx-TestContextAreaFixedMargins PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextAreaFixedRect "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextAreaFixedRect" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextAreaFixedRect.png")
set_tests_properties(vtkChartsCoreCxx-TestContextAreaFixedRect PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextImage "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextImage" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextImage.png")
set_tests_properties(vtkChartsCoreCxx-TestContextImage PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextItemStacking "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextItemStacking" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextItemStacking.png")
set_tests_properties(vtkChartsCoreCxx-TestContextItemStacking PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestContextUnicode "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestContextUnicode" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Fonts/DejaVuSans.ttf" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestContextUnicode.png")
set_tests_properties(vtkChartsCoreCxx-TestContextUnicode PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestControlPointsHandleItem "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestControlPointsHandleItem" "-E" "30" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestControlPointsHandleItem.png")
set_tests_properties(vtkChartsCoreCxx-TestControlPointsHandleItem PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestDiagram "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestDiagram" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestDiagram.png")
set_tests_properties(vtkChartsCoreCxx-TestDiagram PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestFunctionalBagPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestFunctionalBagPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestFunctionalBagPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestFunctionalBagPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestHistogram2D "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestHistogram2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestHistogram2D.png")
set_tests_properties(vtkChartsCoreCxx-TestHistogram2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestInteractiveChartXYZ "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestInteractiveChartXYZ" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestInteractiveChartXYZ.png")
set_tests_properties(vtkChartsCoreCxx-TestInteractiveChartXYZ PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLegendHiddenPlots "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLegendHiddenPlots" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLegendHiddenPlots.png")
set_tests_properties(vtkChartsCoreCxx-TestLegendHiddenPlots PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlot" "-E" "25" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlot.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlotDouble "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlotDouble" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlotDouble.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlotDouble PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlotDouble2 "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlotDouble2" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlotDouble2.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlotDouble2 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlot3D "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlot3D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlot3D.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlot3D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlotAxisFonts "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlotAxisFonts" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlotAxisFonts.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlotAxisFonts PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlot2 "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlot2" "-E" "25" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlot2.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlot2 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlotInteraction "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlotInteraction" "-E" "25" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlotInteraction.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlotInteraction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlotSelection "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlotSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlotSelection.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlotSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestLinePlotSelection2 "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestLinePlotSelection2" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestLinePlotSelection2.png")
set_tests_properties(vtkChartsCoreCxx-TestLinePlotSelection2 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestMultipleChartRenderers "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestMultipleChartRenderers" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestMultipleChartRenderers.png")
set_tests_properties(vtkChartsCoreCxx-TestMultipleChartRenderers PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestMultipleRenderers "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestMultipleRenderers" "-E" "25" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestMultipleRenderers.png")
set_tests_properties(vtkChartsCoreCxx-TestMultipleRenderers PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestMultipleScalarsToColors "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestMultipleScalarsToColors" "-E" "25" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestMultipleScalarsToColors.png")
set_tests_properties(vtkChartsCoreCxx-TestMultipleScalarsToColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestParallelCoordinates "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestParallelCoordinates" "-E" "15" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestParallelCoordinates.png")
set_tests_properties(vtkChartsCoreCxx-TestParallelCoordinates PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestParallelCoordinatesDouble "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestParallelCoordinatesDouble" "-E" "15" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestParallelCoordinatesDouble.png")
set_tests_properties(vtkChartsCoreCxx-TestParallelCoordinatesDouble PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestPieChart "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestPieChart" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestPieChart.png")
set_tests_properties(vtkChartsCoreCxx-TestPieChart PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestPlotMatrix "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestPlotMatrix" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestPlotMatrix.png")
set_tests_properties(vtkChartsCoreCxx-TestPlotMatrix PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestPropItem "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestPropItem" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestPropItem.png")
set_tests_properties(vtkChartsCoreCxx-TestPropItem PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestScalarsToColors "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestScalarsToColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestScalarsToColors.png")
set_tests_properties(vtkChartsCoreCxx-TestScalarsToColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestScatterPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestScatterPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestScatterPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestScatterPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestScatterPlotMatrix "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestScatterPlotMatrix" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestScatterPlotMatrix.png")
set_tests_properties(vtkChartsCoreCxx-TestScatterPlotMatrix PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestScatterPlotMatrixVehicles "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestScatterPlotMatrixVehicles" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestScatterPlotMatrixVehicles.png")
set_tests_properties(vtkChartsCoreCxx-TestScatterPlotMatrixVehicles PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestScatterPlotMatrixVisible "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestScatterPlotMatrixVisible" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestScatterPlotMatrixVisible.png")
set_tests_properties(vtkChartsCoreCxx-TestScatterPlotMatrixVisible PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestScientificPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestScientificPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestScientificPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestScientificPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestStackedBarGraph "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestStackedBarGraph" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestStackedBarGraph.png")
set_tests_properties(vtkChartsCoreCxx-TestStackedBarGraph PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestStackedPlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestStackedPlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestStackedPlot.png")
set_tests_properties(vtkChartsCoreCxx-TestStackedPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestSurfacePlot "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestSurfacePlot" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestSurfacePlot.png")
set_tests_properties(vtkChartsCoreCxx-TestSurfacePlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkChartsCoreCxx-TestZoomAxis "/usr/src/Paraview5/bin/vtkChartsCoreCxxTests" "TestZoomAxis" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Charts/Core/Testing/Data/Baseline/TestZoomAxis.png")
set_tests_properties(vtkChartsCoreCxx-TestZoomAxis PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
