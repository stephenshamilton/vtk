# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Charts/Core
# Build directory: /usr/src/Paraview5/VTK/Charts/Core
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkChartsCore-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Charts/Core" "VTKCHARTSCORE_EXPORT")
set_tests_properties(vtkChartsCore-HeaderTest PROPERTIES  LABELS "")
