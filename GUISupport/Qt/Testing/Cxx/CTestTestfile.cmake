# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/GUISupport/Qt/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/GUISupport/Qt/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkGUISupportQtCxx-TestQtDebugLeaksView "/usr/src/Paraview5/bin/vtkGUISupportQtCxxTests" "TestQtDebugLeaksView" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkGUISupportQtCxx-TestQtDebugLeaksView PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkGUISupportQtCxx-TestQtTableModelAdapter "/usr/src/Paraview5/bin/vtkGUISupportQtCxxTests" "TestQtTableModelAdapter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkGUISupportQtCxx-TestQtTableModelAdapter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkGUISupportQtCxx-TestQtTreeModelAdapter "/usr/src/Paraview5/bin/vtkGUISupportQtCxxTests" "TestQtTreeModelAdapter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkGUISupportQtCxx-TestQtTreeModelAdapter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
