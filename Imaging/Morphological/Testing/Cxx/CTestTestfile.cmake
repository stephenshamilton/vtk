# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Imaging/Morphological/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Imaging/Morphological/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkImagingMorphologicalCxx-TestImageThresholdConnectivity "/usr/src/Paraview5/bin/vtkImagingMorphologicalCxxTests" "TestImageThresholdConnectivity" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Imaging/Morphological/Testing/Data/Baseline/TestImageThresholdConnectivity.png")
set_tests_properties(vtkImagingMorphologicalCxx-TestImageThresholdConnectivity PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
