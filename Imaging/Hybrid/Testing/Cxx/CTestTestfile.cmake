# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Imaging/Hybrid/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Imaging/Hybrid/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkImagingHybridCxx-TestSampleFunction "/usr/src/Paraview5/bin/vtkImagingHybridCxxTests" "TestSampleFunction")
set_tests_properties(vtkImagingHybridCxx-TestSampleFunction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
