# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Domains/Chemistry/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Domains/Chemistry/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkDomainsChemistryCxx-TestBallAndStick "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestBallAndStick" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestBallAndStick.png")
set_tests_properties(vtkDomainsChemistryCxx-TestBallAndStick PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestPDBBallAndStick "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestPDBBallAndStick" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestPDBBallAndStick.png")
set_tests_properties(vtkDomainsChemistryCxx-TestPDBBallAndStick PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "" RUN_SERIAL "ON" TIMEOUT "360")
add_test(vtkDomainsChemistryCxx-TestBondColorModeDiscreteByAtom "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestBondColorModeDiscreteByAtom" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestBondColorModeDiscreteByAtom.png")
set_tests_properties(vtkDomainsChemistryCxx-TestBondColorModeDiscreteByAtom PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestBondColorModeSingleColor "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestBondColorModeSingleColor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestBondColorModeSingleColor.png")
set_tests_properties(vtkDomainsChemistryCxx-TestBondColorModeSingleColor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestCompositeRender "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestCompositeRender" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestCompositeRender.png")
set_tests_properties(vtkDomainsChemistryCxx-TestCompositeRender PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestFastRender "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestFastRender" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestFastRender.png")
set_tests_properties(vtkDomainsChemistryCxx-TestFastRender PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestLiquoriceSticks "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestLiquoriceSticks" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestLiquoriceSticks.png")
set_tests_properties(vtkDomainsChemistryCxx-TestLiquoriceSticks PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestMolecule "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestMolecule" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkDomainsChemistryCxx-TestMolecule PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestMoleculeSelection "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestMoleculeSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkDomainsChemistryCxx-TestMoleculeSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestMoleculeMapperPropertyUpdate "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestMoleculeMapperPropertyUpdate" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestMoleculeMapperPropertyUpdate.png")
set_tests_properties(vtkDomainsChemistryCxx-TestMoleculeMapperPropertyUpdate PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestMultiCylinderOn "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestMultiCylinderOn" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestMultiCylinderOn.png")
set_tests_properties(vtkDomainsChemistryCxx-TestMultiCylinderOn PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestMultiCylinderOff "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestMultiCylinderOff" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestMultiCylinderOff.png")
set_tests_properties(vtkDomainsChemistryCxx-TestMultiCylinderOff PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestPeriodicTable "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestPeriodicTable" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkDomainsChemistryCxx-TestPeriodicTable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestProgrammableElectronicData "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestProgrammableElectronicData" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkDomainsChemistryCxx-TestProgrammableElectronicData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestProteinRibbon "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestProteinRibbon" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestProteinRibbon.png")
set_tests_properties(vtkDomainsChemistryCxx-TestProteinRibbon PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestSimpleBondPerceiver "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestSimpleBondPerceiver" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkDomainsChemistryCxx-TestSimpleBondPerceiver PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestVDWSpheres "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestVDWSpheres" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestVDWSpheres.png")
set_tests_properties(vtkDomainsChemistryCxx-TestVDWSpheres PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkDomainsChemistryCxx-TestCMLMoleculeReader "/usr/src/Paraview5/bin/vtkDomainsChemistryCxxTests" "TestCMLMoleculeReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Domains/Chemistry/Testing/Data/Baseline/TestCMLMoleculeReader.png")
set_tests_properties(vtkDomainsChemistryCxx-TestCMLMoleculeReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
