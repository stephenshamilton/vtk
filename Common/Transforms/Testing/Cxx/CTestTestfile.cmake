# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/Transforms/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/Transforms/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonTransformsCxx-TestTransform "/usr/src/Paraview5/bin/vtkCommonTransformsCxxTests" "TestTransform")
set_tests_properties(vtkCommonTransformsCxx-TestTransform PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
