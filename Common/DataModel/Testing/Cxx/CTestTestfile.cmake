# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/DataModel/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/DataModel/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonDataModelCxx-TestColor "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestColor")
set_tests_properties(vtkCommonDataModelCxx-TestColor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestVector "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestVector")
set_tests_properties(vtkCommonDataModelCxx-TestVector PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestVectorOperators "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestVectorOperators")
set_tests_properties(vtkCommonDataModelCxx-TestVectorOperators PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestAMRBox "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestAMRBox")
set_tests_properties(vtkCommonDataModelCxx-TestAMRBox PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestBiQuadraticQuad "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestBiQuadraticQuad")
set_tests_properties(vtkCommonDataModelCxx-TestBiQuadraticQuad PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestCompositeDataSets "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestCompositeDataSets")
set_tests_properties(vtkCommonDataModelCxx-TestCompositeDataSets PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestComputeBoundingSphere "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestComputeBoundingSphere")
set_tests_properties(vtkCommonDataModelCxx-TestComputeBoundingSphere PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestDataArrayDispatcher "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestDataArrayDispatcher")
set_tests_properties(vtkCommonDataModelCxx-TestDataArrayDispatcher PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestDataObject "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestDataObject")
set_tests_properties(vtkCommonDataModelCxx-TestDataObject PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestDispatchers "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestDispatchers")
set_tests_properties(vtkCommonDataModelCxx-TestDispatchers PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestGenericCell "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestGenericCell")
set_tests_properties(vtkCommonDataModelCxx-TestGenericCell PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestGraph "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestGraph")
set_tests_properties(vtkCommonDataModelCxx-TestGraph PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestGraph2 "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestGraph2")
set_tests_properties(vtkCommonDataModelCxx-TestGraph2 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestGraphAttributes "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestGraphAttributes")
set_tests_properties(vtkCommonDataModelCxx-TestGraphAttributes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestHigherOrderCell "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestHigherOrderCell")
set_tests_properties(vtkCommonDataModelCxx-TestHigherOrderCell PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestImageDataFindCell "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestImageDataFindCell")
set_tests_properties(vtkCommonDataModelCxx-TestImageDataFindCell PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestImageDataInterpolation "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestImageDataInterpolation")
set_tests_properties(vtkCommonDataModelCxx-TestImageDataInterpolation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestImageIterator "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestImageIterator")
set_tests_properties(vtkCommonDataModelCxx-TestImageIterator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestInterpolationDerivs "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestInterpolationDerivs")
set_tests_properties(vtkCommonDataModelCxx-TestInterpolationDerivs PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestInterpolationFunctions "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestInterpolationFunctions")
set_tests_properties(vtkCommonDataModelCxx-TestInterpolationFunctions PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPath "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPath")
set_tests_properties(vtkCommonDataModelCxx-TestPath PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPentagonalPrism "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPentagonalPrism")
set_tests_properties(vtkCommonDataModelCxx-TestPentagonalPrism PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPixelExtent "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPixelExtent")
set_tests_properties(vtkCommonDataModelCxx-TestPixelExtent PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPointLocators "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPointLocators")
set_tests_properties(vtkCommonDataModelCxx-TestPointLocators PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPolyDataRemoveCell "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPolyDataRemoveCell")
set_tests_properties(vtkCommonDataModelCxx-TestPolyDataRemoveCell PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPolygon "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPolygon")
set_tests_properties(vtkCommonDataModelCxx-TestPolygon PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPolyhedron0 "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPolyhedron0")
set_tests_properties(vtkCommonDataModelCxx-TestPolyhedron0 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPolyhedron1 "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPolyhedron1")
set_tests_properties(vtkCommonDataModelCxx-TestPolyhedron1 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestQuadraticPolygon "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestQuadraticPolygon")
set_tests_properties(vtkCommonDataModelCxx-TestQuadraticPolygon PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestRect "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestRect")
set_tests_properties(vtkCommonDataModelCxx-TestRect PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestSelectionSubtract "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestSelectionSubtract")
set_tests_properties(vtkCommonDataModelCxx-TestSelectionSubtract PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestSortFieldData "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestSortFieldData")
set_tests_properties(vtkCommonDataModelCxx-TestSortFieldData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestTable "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestTable")
set_tests_properties(vtkCommonDataModelCxx-TestTable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestTreeBFSIterator "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestTreeBFSIterator")
set_tests_properties(vtkCommonDataModelCxx-TestTreeBFSIterator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestTreeDFSIterator "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestTreeDFSIterator")
set_tests_properties(vtkCommonDataModelCxx-TestTreeDFSIterator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestTriangle "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestTriangle")
set_tests_properties(vtkCommonDataModelCxx-TestTriangle PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherCellArray "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherCellArray")
set_tests_properties(vtkCommonDataModelCxx-otherCellArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherCellBoundaries "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherCellBoundaries")
set_tests_properties(vtkCommonDataModelCxx-otherCellBoundaries PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherCellPosition "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherCellPosition")
set_tests_properties(vtkCommonDataModelCxx-otherCellPosition PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherCellTypes "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherCellTypes")
set_tests_properties(vtkCommonDataModelCxx-otherCellTypes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherColorTransferFunction "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherColorTransferFunction")
set_tests_properties(vtkCommonDataModelCxx-otherColorTransferFunction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherEmptyCell "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherEmptyCell")
set_tests_properties(vtkCommonDataModelCxx-otherEmptyCell PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherFieldData "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherFieldData")
set_tests_properties(vtkCommonDataModelCxx-otherFieldData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherRectilinearGrid "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherRectilinearGrid")
set_tests_properties(vtkCommonDataModelCxx-otherRectilinearGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-otherStructuredGrid "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "otherStructuredGrid")
set_tests_properties(vtkCommonDataModelCxx-otherStructuredGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-quadCellConsistency "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "quadCellConsistency")
set_tests_properties(vtkCommonDataModelCxx-quadCellConsistency PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-quadraticEvaluation "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "quadraticEvaluation")
set_tests_properties(vtkCommonDataModelCxx-quadraticEvaluation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestBoundingBox "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestBoundingBox")
set_tests_properties(vtkCommonDataModelCxx-TestBoundingBox PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPlane "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPlane")
set_tests_properties(vtkCommonDataModelCxx-TestPlane PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestStaticCellLinks "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestStaticCellLinks")
set_tests_properties(vtkCommonDataModelCxx-TestStaticCellLinks PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestStructuredData "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestStructuredData")
set_tests_properties(vtkCommonDataModelCxx-TestStructuredData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestDataObjectTypes "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestDataObjectTypes")
set_tests_properties(vtkCommonDataModelCxx-TestDataObjectTypes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestPolyDataRemoveDeletedCells "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestPolyDataRemoveDeletedCells")
set_tests_properties(vtkCommonDataModelCxx-TestPolyDataRemoveDeletedCells PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-UnitTestCells "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "UnitTestCells")
set_tests_properties(vtkCommonDataModelCxx-UnitTestCells PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-UnitTestImplicitDataSet "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "UnitTestImplicitDataSet")
set_tests_properties(vtkCommonDataModelCxx-UnitTestImplicitDataSet PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-UnitTestImplicitVolume "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "UnitTestImplicitVolume")
set_tests_properties(vtkCommonDataModelCxx-UnitTestImplicitVolume PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-UnitTestPlanesIntersection "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "UnitTestPlanesIntersection")
set_tests_properties(vtkCommonDataModelCxx-UnitTestPlanesIntersection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-quadraticIntersection "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "quadraticIntersection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/quadraticIntersection.png")
set_tests_properties(vtkCommonDataModelCxx-quadraticIntersection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestCellIterators "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestCellIterators" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkCommonDataModelCxx-TestCellIterators PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestCellLocator "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestCellLocator" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/TestCellLocator.png")
set_tests_properties(vtkCommonDataModelCxx-TestCellLocator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestMeanValueCoordinatesInterpolation1 "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestMeanValueCoordinatesInterpolation1" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/TestMeanValueCoordinatesInterpolation1.png")
set_tests_properties(vtkCommonDataModelCxx-TestMeanValueCoordinatesInterpolation1 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestMeanValueCoordinatesInterpolation2 "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestMeanValueCoordinatesInterpolation2" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/TestMeanValueCoordinatesInterpolation2.png")
set_tests_properties(vtkCommonDataModelCxx-TestMeanValueCoordinatesInterpolation2 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestSmoothErrorMetric "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestSmoothErrorMetric" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/TestSmoothErrorMetric.png")
set_tests_properties(vtkCommonDataModelCxx-TestSmoothErrorMetric PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestQuadraticPolygonFilters "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestQuadraticPolygonFilters" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/TestQuadraticPolygonFilters.png")
set_tests_properties(vtkCommonDataModelCxx-TestQuadraticPolygonFilters PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonDataModelCxx-TestKdTreeRepresentation "/usr/src/Paraview5/bin/vtkCommonDataModelCxxTests" "TestKdTreeRepresentation" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/DataModel/Testing/Data/Baseline/TestKdTreeRepresentation.png")
set_tests_properties(vtkCommonDataModelCxx-TestKdTreeRepresentation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
