# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/Math/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/Math/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonMathCxx-TestAmoebaMinimizer "/usr/src/Paraview5/bin/vtkCommonMathCxxTests" "TestAmoebaMinimizer")
set_tests_properties(vtkCommonMathCxx-TestAmoebaMinimizer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonMathCxx-TestMatrix3x3 "/usr/src/Paraview5/bin/vtkCommonMathCxxTests" "TestMatrix3x3")
set_tests_properties(vtkCommonMathCxx-TestMatrix3x3 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonMathCxx-TestPolynomialSolversUnivariate "/usr/src/Paraview5/bin/vtkCommonMathCxxTests" "TestPolynomialSolversUnivariate")
set_tests_properties(vtkCommonMathCxx-TestPolynomialSolversUnivariate PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonMathCxx-TestQuaternion "/usr/src/Paraview5/bin/vtkCommonMathCxxTests" "TestQuaternion")
set_tests_properties(vtkCommonMathCxx-TestQuaternion PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
