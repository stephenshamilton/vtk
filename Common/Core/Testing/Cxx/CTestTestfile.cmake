# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/Core/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/Core/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonCoreCxx-UnitTestMath "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "UnitTestMath")
set_tests_properties(vtkCommonCoreCxx-UnitTestMath PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestAngularPeriodicDataArray "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestAngularPeriodicDataArray")
set_tests_properties(vtkCommonCoreCxx-TestAngularPeriodicDataArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayAPI "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayAPI")
set_tests_properties(vtkCommonCoreCxx-TestArrayAPI PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayAPIConvenience "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayAPIConvenience")
set_tests_properties(vtkCommonCoreCxx-TestArrayAPIConvenience PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayAPIDense "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayAPIDense")
set_tests_properties(vtkCommonCoreCxx-TestArrayAPIDense PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayAPISparse "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayAPISparse")
set_tests_properties(vtkCommonCoreCxx-TestArrayAPISparse PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayBool "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayBool")
set_tests_properties(vtkCommonCoreCxx-TestArrayBool PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestAtomic "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestAtomic")
set_tests_properties(vtkCommonCoreCxx-TestAtomic PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestScalarsToColors "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestScalarsToColors")
set_tests_properties(vtkCommonCoreCxx-TestScalarsToColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayExtents "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayExtents")
set_tests_properties(vtkCommonCoreCxx-TestArrayExtents PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayInterpolationDense "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayInterpolationDense")
set_tests_properties(vtkCommonCoreCxx-TestArrayInterpolationDense PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayLookup "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayLookup")
set_tests_properties(vtkCommonCoreCxx-TestArrayLookup PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayNullValues "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayNullValues")
set_tests_properties(vtkCommonCoreCxx-TestArrayNullValues PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArraySize "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArraySize")
set_tests_properties(vtkCommonCoreCxx-TestArraySize PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayUniqueValueDetection "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayUniqueValueDetection")
set_tests_properties(vtkCommonCoreCxx-TestArrayUniqueValueDetection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayUserTypes "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayUserTypes")
set_tests_properties(vtkCommonCoreCxx-TestArrayUserTypes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestArrayVariants "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestArrayVariants")
set_tests_properties(vtkCommonCoreCxx-TestArrayVariants PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestCollection "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestCollection")
set_tests_properties(vtkCommonCoreCxx-TestCollection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestConditionVariable "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestConditionVariable")
set_tests_properties(vtkCommonCoreCxx-TestConditionVariable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestDataArray "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestDataArray")
set_tests_properties(vtkCommonCoreCxx-TestDataArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestDataArrayAPI "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestDataArrayAPI")
set_tests_properties(vtkCommonCoreCxx-TestDataArrayAPI PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestDataArrayComponentNames "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestDataArrayComponentNames")
set_tests_properties(vtkCommonCoreCxx-TestDataArrayComponentNames PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestDataArrayIterators "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestDataArrayIterators")
set_tests_properties(vtkCommonCoreCxx-TestDataArrayIterators PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestGarbageCollector "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestGarbageCollector")
set_tests_properties(vtkCommonCoreCxx-TestGarbageCollector PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestLookupTable "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestLookupTable")
set_tests_properties(vtkCommonCoreCxx-TestLookupTable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestLookupTableThreaded "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestLookupTableThreaded")
set_tests_properties(vtkCommonCoreCxx-TestLookupTableThreaded PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestMath "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestMath")
set_tests_properties(vtkCommonCoreCxx-TestMath PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestMinimalStandardRandomSequence "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestMinimalStandardRandomSequence")
set_tests_properties(vtkCommonCoreCxx-TestMinimalStandardRandomSequence PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestNew "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestNew")
set_tests_properties(vtkCommonCoreCxx-TestNew PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestObjectFactory "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestObjectFactory")
set_tests_properties(vtkCommonCoreCxx-TestObjectFactory PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestObservers "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestObservers")
set_tests_properties(vtkCommonCoreCxx-TestObservers PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestObserversPerformance "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestObserversPerformance")
set_tests_properties(vtkCommonCoreCxx-TestObserversPerformance PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestOStreamWrapper "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestOStreamWrapper")
set_tests_properties(vtkCommonCoreCxx-TestOStreamWrapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestSMP "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestSMP")
set_tests_properties(vtkCommonCoreCxx-TestSMP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestSmartPointer "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestSmartPointer")
set_tests_properties(vtkCommonCoreCxx-TestSmartPointer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestSortDataArray "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestSortDataArray")
set_tests_properties(vtkCommonCoreCxx-TestSortDataArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestSparseArrayValidation "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestSparseArrayValidation")
set_tests_properties(vtkCommonCoreCxx-TestSparseArrayValidation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestSystemInformation "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestSystemInformation" "/usr/src/Paraview5")
set_tests_properties(vtkCommonCoreCxx-TestSystemInformation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestTemplateMacro "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestTemplateMacro")
set_tests_properties(vtkCommonCoreCxx-TestTemplateMacro PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestTimePointUtility "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestTimePointUtility")
set_tests_properties(vtkCommonCoreCxx-TestTimePointUtility PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestUnicodeStringAPI "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestUnicodeStringAPI")
set_tests_properties(vtkCommonCoreCxx-TestUnicodeStringAPI PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestUnicodeStringArrayAPI "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestUnicodeStringArrayAPI")
set_tests_properties(vtkCommonCoreCxx-TestUnicodeStringArrayAPI PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestVariant "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestVariant")
set_tests_properties(vtkCommonCoreCxx-TestVariant PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestVariantComparison "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestVariantComparison")
set_tests_properties(vtkCommonCoreCxx-TestVariantComparison PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestWeakPointer "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestWeakPointer")
set_tests_properties(vtkCommonCoreCxx-TestWeakPointer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-TestXMLFileOutputWindow "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "TestXMLFileOutputWindow" "/usr/src/Paraview5/Testing/Temporary/XMLFileOutputWindow.txt")
set_tests_properties(vtkCommonCoreCxx-TestXMLFileOutputWindow PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-UnitTestInformationKeys "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "UnitTestInformationKeys")
set_tests_properties(vtkCommonCoreCxx-UnitTestInformationKeys PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-otherArrays "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "otherArrays")
set_tests_properties(vtkCommonCoreCxx-otherArrays PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-otherByteSwap "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "otherByteSwap")
set_tests_properties(vtkCommonCoreCxx-otherByteSwap PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonCoreCxx-otherStringArray "/usr/src/Paraview5/bin/vtkCommonCoreCxxTests" "otherStringArray")
set_tests_properties(vtkCommonCoreCxx-otherStringArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
