# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/ComputationalGeometry/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/ComputationalGeometry/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonComputationalGeometryCxx-UnitTestParametricSpline "/usr/src/Paraview5/bin/vtkCommonComputationalGeometryCxxTests" "UnitTestParametricSpline")
set_tests_properties(vtkCommonComputationalGeometryCxx-UnitTestParametricSpline PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
