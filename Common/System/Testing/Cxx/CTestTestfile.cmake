# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/System/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/System/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonSystemCxx-TestDirectory "/usr/src/Paraview5/bin/vtkCommonSystemCxxTests" "TestDirectory")
set_tests_properties(vtkCommonSystemCxx-TestDirectory PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonSystemCxx-otherTimerLog "/usr/src/Paraview5/bin/vtkCommonSystemCxxTests" "otherTimerLog")
set_tests_properties(vtkCommonSystemCxx-otherTimerLog PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
