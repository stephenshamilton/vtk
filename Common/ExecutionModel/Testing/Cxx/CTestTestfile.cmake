# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/ExecutionModel/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/ExecutionModel/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonExecutionModelCxx-TestCopyAttributeData "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "TestCopyAttributeData" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-TestCopyAttributeData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonExecutionModelCxx-TestImageDataToStructuredGrid "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "TestImageDataToStructuredGrid" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-TestImageDataToStructuredGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonExecutionModelCxx-TestMetaData "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "TestMetaData" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-TestMetaData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonExecutionModelCxx-TestSetInputDataObject "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "TestSetInputDataObject" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-TestSetInputDataObject PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonExecutionModelCxx-TestTemporalSupport "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "TestTemporalSupport" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-TestTemporalSupport PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonExecutionModelCxx-TestTrivialConsumer "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "TestTrivialConsumer" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-TestTrivialConsumer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonExecutionModelCxx-UnitTestSimpleScalarTree "/usr/src/Paraview5/bin/vtkCommonExecutionModelCxxTests" "UnitTestSimpleScalarTree" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonExecutionModelCxx-UnitTestSimpleScalarTree PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
