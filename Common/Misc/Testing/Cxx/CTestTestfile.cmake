# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/Misc/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/Misc/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonMiscCxx-TestPolygonBuilder "/usr/src/Paraview5/bin/vtkCommonMiscCxxTests" "TestPolygonBuilder")
set_tests_properties(vtkCommonMiscCxx-TestPolygonBuilder PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonMiscCxx-UnitTestFunctionParser "/usr/src/Paraview5/bin/vtkCommonMiscCxxTests" "UnitTestFunctionParser")
set_tests_properties(vtkCommonMiscCxx-UnitTestFunctionParser PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
