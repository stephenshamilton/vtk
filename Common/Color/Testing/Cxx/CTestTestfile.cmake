# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Common/Color/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Common/Color/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkCommonColorCxx-TestCategoricalColors "/usr/src/Paraview5/bin/vtkCommonColorCxxTests" "TestCategoricalColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonColorCxx-TestCategoricalColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonColorCxx-TestColorSeries "/usr/src/Paraview5/bin/vtkCommonColorCxxTests" "TestColorSeries" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Common/Color/Testing/Data/Baseline/TestColorSeries.png")
set_tests_properties(vtkCommonColorCxx-TestColorSeries PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonColorCxx-TestColorSeriesLookupTables "/usr/src/Paraview5/bin/vtkCommonColorCxxTests" "TestColorSeriesLookupTables" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonColorCxx-TestColorSeriesLookupTables PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkCommonColorCxx-TestNamedColors "/usr/src/Paraview5/bin/vtkCommonColorCxxTests" "TestNamedColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkCommonColorCxx-TestNamedColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
