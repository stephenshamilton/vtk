cmake_minimum_required(VERSION 2.4.4)
set(CMAKE_ALLOW_LOOSE_LOOP_CONSTRUCTS ON)

project(zfp C)

set(VERSION "0.5.0")

if (FALSE) # XXX(kitware): remove these options from the cache.
option(ASM686 "Enable building i686 assembly implementation")
option(AMD64 "Enable building amd64 assembly implementation")
endif ()

set(INSTALL_BIN_DIR "${CMAKE_INSTALL_PREFIX}/bin" CACHE PATH "Installation directory for executables")
set(INSTALL_LIB_DIR "${CMAKE_INSTALL_PREFIX}/lib" CACHE PATH "Installation directory for libraries")
set(INSTALL_INC_DIR "${CMAKE_INSTALL_PREFIX}/include" CACHE PATH "Installation directory for headers")
set(INSTALL_MAN_DIR "${CMAKE_INSTALL_PREFIX}/share/man" CACHE PATH "Installation directory for manual pages")
set(INSTALL_PKGCONFIG_DIR "${CMAKE_INSTALL_PREFIX}/share/pkgconfig" CACHE PATH "Installation directory for pkgconfig (.pc) files")

# XXX(kitware): override the installation directory.
set(INSTALL_INC_DIR "${VTK_INSTALL_INCLUDE_DIR}/vtkzfp")

include(CheckTypeSize)
include(CheckFunctionExists)
include(CheckIncludeFile)
include(CheckCSourceCompiles)
enable_testing()

check_include_file(sys/types.h HAVE_SYS_TYPES_H)
check_include_file(stdint.h    HAVE_STDINT_H)
check_include_file(stddef.h    HAVE_STDDEF_H)

#
# Check to see if we have large file support
#
set(CMAKE_REQUIRED_DEFINITIONS -D_LARGEFILE64_SOURCE=1)
# We add these other definitions here because CheckTypeSize.cmake
# in CMake 2.4.x does not automatically do so and we want
# compatibility with CMake 2.4.x.
if(HAVE_SYS_TYPES_H)
    list(APPEND CMAKE_REQUIRED_DEFINITIONS -DHAVE_SYS_TYPES_H)
endif()
if(HAVE_STDINT_H)
    list(APPEND CMAKE_REQUIRED_DEFINITIONS -DHAVE_STDINT_H)
endif()
if(HAVE_STDDEF_H)
    list(APPEND CMAKE_REQUIRED_DEFINITIONS -DHAVE_STDDEF_H)
endif()
check_type_size(off64_t OFF64_T)
if(HAVE_OFF64_T)
   add_definitions(-D_LARGEFILE64_SOURCE=1)
endif()
set(CMAKE_REQUIRED_DEFINITIONS) # clear variable

#
# Check for fseeko
#
check_function_exists(fseeko HAVE_FSEEKO)
if(NOT HAVE_FSEEKO)
    add_definitions(-DNO_FSEEKO)
endif()

#
# Check for unistd.h
#
check_include_file(unistd.h Z_HAVE_UNISTD_H)

if(MSVC)
    if (FALSE) # XXX(kitware): vtk doesn't use the "d" suffix
    set(CMAKE_DEBUG_POSTFIX "d")
    endif ()
    add_definitions(-D_CRT_SECURE_NO_DEPRECATE)
    add_definitions(-D_CRT_NONSTDC_NO_DEPRECATE)
    include_directories(${CMAKE_CURRENT_SOURCE_DIR})
endif()

if(NOT CMAKE_CURRENT_SOURCE_DIR STREQUAL CMAKE_CURRENT_BINARY_DIR)
    # If we're doing an out of source build and the user has a zconf.h
    # in their source tree...
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/zconf.h)
        message(STATUS "Renaming")
        message(STATUS "    ${CMAKE_CURRENT_SOURCE_DIR}/zconf.h")
        message(STATUS "to 'zconf.h.included' because this file is included with zlib")
        message(STATUS "but CMake generates it automatically in the build directory.")
        file(RENAME ${CMAKE_CURRENT_SOURCE_DIR}/zconf.h ${CMAKE_CURRENT_SOURCE_DIR}/zconf.h.included)
  endif()
endif()

# XXX(kitware): always mangle
set(Z_PREFIX TRUE)

if (FALSE) # XXX(kitware): the pkgconfig file isn't necessary.
set(ZFP_PC ${CMAKE_CURRENT_BINARY_DIR}/zfp.pc)
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/zfp.pc.cmakein
		${ZFP_PC} @ONLY)
endif ()
####The following was for zlib.  Does ZFP need one?
#configure_file(	${CMAKE_CURRENT_SOURCE_DIR}/zconf.h.cmakein
#		${CMAKE_CURRENT_BINARY_DIR}/zconf.h @ONLY)
#include_directories(${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_SOURCE_DIR})


#============================================================================
# zfp
#============================================================================

#Math library is required.
link_libraries(m)

set(ZFP_PUBLIC_HDRS
    #${CMAKE_CURRENT_BINARY_DIR}/zconf.h
    #zlib.h
    inc/bitstream.h
    inc/macros.h
    inc/system.h
    inc/types.h
    inc/zfp.h
)
set(ZFP_PRIVATE_HDRS
    src/block1.h
    src/block2.h
    src/block3.h
    src/traitsd.h
    src/traitsf.h
)
set(ZFP_SRCS
    src/bitstream.c
    src/decode1f.c
    src/decode2f.c
    src/decode3f.c
    src/encode1f.c
    src/encode2f.c
    src/encode3f.c
    src/decode1d.c
    src/decode2d.c
    src/decode3d.c
    src/encode1d.c
    src/encode2d.c
    src/encode3d.c
    src/zfp.c
)


# XXX(kitware): create the third-party library for VTK
vtk_add_library(vtkzfp ${ZFP_SRCS} ${ZFP_ASMS} ${ZFP_DLL_SRCS} ${ZFP_PUBLIC_HDRS} ${ZFP_PRIVATE_HDRS})
set_target_properties(vtkzfp PROPERTIES DEFINE_SYMBOL ZFP_DLL)

if (NOT VTK_INSTALL_NO_DEVELOPMENT)
    install(FILES
        ${ZFP_PUBLIC_HDRS}
        DESTINATION "${INSTALL_INC_DIR}"
        COMPONENT Development)
endif ()

if (FALSE) # XXX(kitware): skip all the original build/install commands

if(NOT MINGW)
    set(ZFP_DLL_SRCS
        win32/zfp.rc # If present will override custom build rule below.
    )
endif()

if(CMAKE_COMPILER_IS_GNUCC)
    if(ASM686)
        set(ZFP_ASMS contrib/asm686/match.S)
    elseif (AMD64)
        set(ZFP_ASMS contrib/amd64/amd64-match.S)
    endif ()

	if(ZFP_ASMS)
		add_definitions(-DASMV)
		set_source_files_properties(${ZFP_ASMS} PROPERTIES LANGUAGE C COMPILE_FLAGS -DNO_UNDERLINE)
	endif()
endif()

if(MSVC)
    if(ASM686)
		ENABLE_LANGUAGE(ASM_MASM)
        set(ZFP_ASMS
			contrib/masmx86/inffas32.asm
			contrib/masmx86/match686.asm
		)
    elseif (AMD64)
		ENABLE_LANGUAGE(ASM_MASM)
        set(ZFP_ASMS
			contrib/masmx64/gvmat64.asm
			contrib/masmx64/inffasx64.asm
		)
    endif()

	if(ZFP_ASMS)
		add_definitions(-DASMV -DASMINF)
	endif()
endif()

# parse the full version number from zlib.h and include in ZLIB_FULL_VERSION
file(READ ${CMAKE_CURRENT_SOURCE_DIR}/zfp.h _zfp_h_contents)
string(REGEX REPLACE ".*#define[ \t]+ZLIB_VERSION[ \t]+\"([-0-9A-Za-z.]+)\".*"
    "\\1" ZLIB_FULL_VERSION ${_zfp_h_contents})

if(MINGW)
    # This gets us DLL resource information when compiling on MinGW.
    if(NOT CMAKE_RC_COMPILER)
        set(CMAKE_RC_COMPILER windres.exe)
    endif()

    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/zfprc.obj
                       COMMAND ${CMAKE_RC_COMPILER}
                            -D GCC_WINDRES
                            -I ${CMAKE_CURRENT_SOURCE_DIR}
                            -I ${CMAKE_CURRENT_BINARY_DIR}
                            -o ${CMAKE_CURRENT_BINARY_DIR}/zfprc.obj
                            -i ${CMAKE_CURRENT_SOURCE_DIR}/win32/zfp.rc)
    set(ZLIB_DLL_SRCS ${CMAKE_CURRENT_BINARY_DIR}/zfprc.obj)
endif(MINGW)

add_library(zfp SHARED ${ZFP_SRCS} ${ZFP_ASMS} ${ZFP_DLL_SRCS} ${ZFP_PUBLIC_HDRS} ${ZFP_PRIVATE_HDRS})
add_library(zfpstatic STATIC ${ZFP_SRCS} ${ZFP_ASMS} ${ZFP_PUBLIC_HDRS} ${ZFP_PRIVATE_HDRS})
set_target_properties(zfp PROPERTIES DEFINE_SYMBOL ZFP_DLL)
set_target_properties(zfp PROPERTIES SOVERSION 1)



if(NOT CYGWIN)
    # This property causes shared libraries on Linux to have the full version
    # encoded into their final filename.  We disable this on Cygwin because
    # it causes cygz-${ZLIB_FULL_VERSION}.dll to be created when cygz.dll
    # seems to be the default.
    #
    # This has no effect with MSVC, on that platform the version info for
    # the DLL comes from the resource file win32/zlib1.rc
    set_target_properties(zfp PROPERTIES VERSION ${ZFP_FULL_VERSION})
endif()

if(UNIX)
    # On unix-like platforms the library is almost always called libz
   set_target_properties(zfp zlibstatic PROPERTIES OUTPUT_NAME z)
   if(NOT APPLE)
     set_target_properties(zfp PROPERTIES LINK_FLAGS "-Wl,--version-script,\"${CMAKE_CURRENT_SOURCE_DIR}/zfp.map\"")
   endif()
elseif(BUILD_SHARED_LIBS AND WIN32)
    # Creates zfp.dll when building shared library version
    set_target_properties(zfp PROPERTIES SUFFIX ".dll")
endif()

if(NOT SKIP_INSTALL_LIBRARIES AND NOT SKIP_INSTALL_ALL )
    install(TARGETS zfp zfpstatic
        RUNTIME DESTINATION "${INSTALL_BIN_DIR}"
        ARCHIVE DESTINATION "${INSTALL_LIB_DIR}"
        LIBRARY DESTINATION "${INSTALL_LIB_DIR}" )
endif()
if(NOT SKIP_INSTALL_HEADERS AND NOT SKIP_INSTALL_ALL )
    install(FILES ${ZFP_PUBLIC_HDRS} DESTINATION "${INSTALL_INC_DIR}")
endif()
if(NOT SKIP_INSTALL_FILES AND NOT SKIP_INSTALL_ALL )
    install(FILES zfp.3 DESTINATION "${INSTALL_MAN_DIR}/man3")
endif()
if(NOT SKIP_INSTALL_FILES AND NOT SKIP_INSTALL_ALL )
    install(FILES ${ZFP_PC} DESTINATION "${INSTALL_PKGCONFIG_DIR}")
endif()

#============================================================================
# Example binaries
#============================================================================

add_executable(example examples/testzfp.cpp)
#target_link_libraries(example zfp)
add_test(example example)

#add_executable(minigzip test/minigzip.c)
#target_link_libraries(minigzip zfp)

#if(HAVE_OFF64_T)
#    add_executable(example64 test/example.c)
#    target_link_libraries(example64 zlib)
#    set_target_properties(example64 PROPERTIES COMPILE_FLAGS "-D_FILE_OFFSET_BITS=64")
#    add_test(example64 example64)

#    add_executable(minigzip64 test/minigzip.c)
#    target_link_libraries(minigzip64 zlib)
#    set_target_properties(minigzip64 PROPERTIES COMPILE_FLAGS "-D_FILE_OFFSET_BITS=64")
#endif()

endif ()
