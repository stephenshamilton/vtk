# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/ThirdParty/netcdf/vtknetcdf
# Build directory: /usr/src/Paraview5/VTK/ThirdParty/netcdf/vtknetcdf
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(liblib)
subdirs(libdispatch)
subdirs(libsrc)
subdirs(libsrc4)
subdirs(cxx)
