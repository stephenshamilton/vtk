# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Parallel/Core/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Parallel/Core/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkParallelCoreCxx-TestFieldDataSerialization "/usr/src/Paraview5/bin/vtkParallelCoreCxxTests" "TestFieldDataSerialization")
set_tests_properties(vtkParallelCoreCxx-TestFieldDataSerialization PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkParallelCore-TestSocketCommunicator "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/CMake/vtkTestDriver.py" "--process" "/usr/src/Paraview5/bin/vtkParallelCore-TestSocketCommunicator" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "--server" "--process" "/usr/src/Paraview5/bin/vtkParallelCore-TestSocketCommunicator" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
