# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Parallel/Core
# Build directory: /usr/src/Paraview5/VTK/Parallel/Core
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkParallelCore-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Parallel/Core" "VTKPARALLELCORE_EXPORT")
set_tests_properties(vtkParallelCore-HeaderTest PROPERTIES  LABELS "")
