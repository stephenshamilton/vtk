# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/VolumeAMR
# Build directory: /usr/src/Paraview5/VTK/Rendering/VolumeAMR
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingVolumeAMR-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Rendering/VolumeAMR" "VTKRENDERINGVOLUMEAMR_EXPORT")
set_tests_properties(vtkRenderingVolumeAMR-HeaderTest PROPERTIES  LABELS "")
