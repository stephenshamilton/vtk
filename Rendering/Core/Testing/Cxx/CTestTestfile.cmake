# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/Core/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Rendering/Core/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingCoreCxx-otherCoordinate "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "otherCoordinate" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-otherCoordinate PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-FrustumClip "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "FrustumClip" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/FrustumClip.png")
set_tests_properties(vtkRenderingCoreCxx-FrustumClip PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-Mace "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "Mace" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/Mace.png")
set_tests_properties(vtkRenderingCoreCxx-Mace PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-RGrid "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "RGrid" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/RGrid.png")
set_tests_properties(vtkRenderingCoreCxx-RGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestActor2D "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestActor2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestActor2D.png")
set_tests_properties(vtkRenderingCoreCxx-TestActor2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestActor2DTextures "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestActor2DTextures" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestActor2DTextures.png")
set_tests_properties(vtkRenderingCoreCxx-TestActor2DTextures PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestActorLightingFlag "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestActorLightingFlag" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestActorLightingFlag.png")
set_tests_properties(vtkRenderingCoreCxx-TestActorLightingFlag PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestAnimationScene "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestAnimationScene" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestAnimationScene.png")
set_tests_properties(vtkRenderingCoreCxx-TestAnimationScene PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestAssemblyBounds "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestAssemblyBounds" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestAssemblyBounds PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestBackfaceCulling "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestBackfaceCulling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestBackfaceCulling.png")
set_tests_properties(vtkRenderingCoreCxx-TestBackfaceCulling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestBareScalarsToColors "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestBareScalarsToColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestBareScalarsToColors.png")
set_tests_properties(vtkRenderingCoreCxx-TestBareScalarsToColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestBlockOpacity "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestBlockOpacity" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestBlockOpacity.png")
set_tests_properties(vtkRenderingCoreCxx-TestBlockOpacity PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestColorByCellDataStringArray "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestColorByCellDataStringArray" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestColorByCellDataStringArray.png")
set_tests_properties(vtkRenderingCoreCxx-TestColorByCellDataStringArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestColorByPointDataStringArray "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestColorByPointDataStringArray" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestColorByPointDataStringArray.png")
set_tests_properties(vtkRenderingCoreCxx-TestColorByPointDataStringArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestColorByStringArrayDefaultLookupTable "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestColorByStringArrayDefaultLookupTable" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestColorByStringArrayDefaultLookupTable.png")
set_tests_properties(vtkRenderingCoreCxx-TestColorByStringArrayDefaultLookupTable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestColorByStringArrayDefaultLookupTable2D "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestColorByStringArrayDefaultLookupTable2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestColorByStringArrayDefaultLookupTable2D.png")
set_tests_properties(vtkRenderingCoreCxx-TestColorByStringArrayDefaultLookupTable2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestColorTransferFunction "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestColorTransferFunction" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestColorTransferFunction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestColorTransferFunctionStringArray "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestColorTransferFunctionStringArray" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestColorTransferFunctionStringArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestDirectScalarsToColors "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestDirectScalarsToColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestDirectScalarsToColors.png")
set_tests_properties(vtkRenderingCoreCxx-TestDirectScalarsToColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestDiscretizableColorTransferFunction "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestDiscretizableColorTransferFunction" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestDiscretizableColorTransferFunction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestDiscretizableColorTransferFunctionStringArray "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestDiscretizableColorTransferFunctionStringArray" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestDiscretizableColorTransferFunctionStringArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestEdgeFlags "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestEdgeFlags" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestEdgeFlags.png")
set_tests_properties(vtkRenderingCoreCxx-TestEdgeFlags PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestFollowerPicking "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestFollowerPicking" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestFollowerPicking.png")
set_tests_properties(vtkRenderingCoreCxx-TestFollowerPicking PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestGlyph3DMapper "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestGlyph3DMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestGlyph3DMapper.png")
set_tests_properties(vtkRenderingCoreCxx-TestGlyph3DMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestGlyph3DMapperArrow "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestGlyph3DMapperArrow" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestGlyph3DMapperArrow.png")
set_tests_properties(vtkRenderingCoreCxx-TestGlyph3DMapperArrow PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestGlyph3DMapperMasking "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestGlyph3DMapperMasking" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestGlyph3DMapperMasking.png")
set_tests_properties(vtkRenderingCoreCxx-TestGlyph3DMapperMasking PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestGlyph3DMapperOrientationArray "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestGlyph3DMapperOrientationArray" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestGlyph3DMapperOrientationArray.png")
set_tests_properties(vtkRenderingCoreCxx-TestGlyph3DMapperOrientationArray PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestGlyph3DMapperPicking "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestGlyph3DMapperPicking" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestGlyph3DMapperPicking.png")
set_tests_properties(vtkRenderingCoreCxx-TestGlyph3DMapperPicking PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestGradientBackground "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestGradientBackground" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestGradientBackground.png")
set_tests_properties(vtkRenderingCoreCxx-TestGradientBackground PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestHomogeneousTransformOfActor "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestHomogeneousTransformOfActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestHomogeneousTransformOfActor.png")
set_tests_properties(vtkRenderingCoreCxx-TestHomogeneousTransformOfActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestInteractorStyleImageProperty "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestInteractorStyleImageProperty" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestInteractorStyleImageProperty PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestInteractorTimers "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestInteractorTimers" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestInteractorTimers PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestLabeledContourMapper "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestLabeledContourMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestLabeledContourMapper.png")
set_tests_properties(vtkRenderingCoreCxx-TestLabeledContourMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestLabeledContourMapperWithActorMatrix "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestLabeledContourMapperWithActorMatrix" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestLabeledContourMapperWithActorMatrix.png")
set_tests_properties(vtkRenderingCoreCxx-TestLabeledContourMapperWithActorMatrix PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestPickingManager "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestPickingManager" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestPickingManager PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestManyActors "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestManyActors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestManyActors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestMapVectorsAsRGBColors "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestMapVectorsAsRGBColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestMapVectorsAsRGBColors.png")
set_tests_properties(vtkRenderingCoreCxx-TestMapVectorsAsRGBColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestMapVectorsToColors "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestMapVectorsToColors" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestMapVectorsToColors.png")
set_tests_properties(vtkRenderingCoreCxx-TestMapVectorsToColors PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestOffAxisStereo "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestOffAxisStereo" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestOffAxisStereo.png")
set_tests_properties(vtkRenderingCoreCxx-TestOffAxisStereo PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestOrderedTriangulator "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestOrderedTriangulator" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestOrderedTriangulator.png")
set_tests_properties(vtkRenderingCoreCxx-TestOrderedTriangulator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestOpacity "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestOpacity" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestOpacity.png")
set_tests_properties(vtkRenderingCoreCxx-TestOpacity PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestOSConeCxx "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestOSConeCxx" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestOSConeCxx.png")
set_tests_properties(vtkRenderingCoreCxx-TestOSConeCxx PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestPointSelection "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestPointSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestPointSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestPolygonSelection "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestPolygonSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestPolygonSelection.png")
set_tests_properties(vtkRenderingCoreCxx-TestPolygonSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestResetCameraVerticalAspectRatio "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestResetCameraVerticalAspectRatio" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestResetCameraVerticalAspectRatio.png")
set_tests_properties(vtkRenderingCoreCxx-TestResetCameraVerticalAspectRatio PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestResetCameraVerticalAspectRatioParallel "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestResetCameraVerticalAspectRatioParallel" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestResetCameraVerticalAspectRatioParallel.png")
set_tests_properties(vtkRenderingCoreCxx-TestResetCameraVerticalAspectRatioParallel PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestSplitViewportStereoHorizontal "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestSplitViewportStereoHorizontal" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestSplitViewportStereoHorizontal.png")
set_tests_properties(vtkRenderingCoreCxx-TestSplitViewportStereoHorizontal PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTexturedBackground "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTexturedBackground" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTexturedBackground.png")
set_tests_properties(vtkRenderingCoreCxx-TestTexturedBackground PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTextureSize "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTextureSize" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-TestTextureSize PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTextureRGBA "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTextureRGBA" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTextureRGBA.png")
set_tests_properties(vtkRenderingCoreCxx-TestTextureRGBA PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTextureRGBADepthPeeling "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTextureRGBADepthPeeling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTextureRGBADepthPeeling.png")
set_tests_properties(vtkRenderingCoreCxx-TestTextureRGBADepthPeeling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTilingCxx "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTilingCxx" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTilingCxx.png")
set_tests_properties(vtkRenderingCoreCxx-TestTilingCxx PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTransformCoordinateUseDouble "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTransformCoordinateUseDouble" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTransformCoordinateUseDouble.png")
set_tests_properties(vtkRenderingCoreCxx-TestTransformCoordinateUseDouble PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTranslucentImageActorAlphaBlending "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTranslucentImageActorAlphaBlending" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTranslucentImageActorAlphaBlending.png")
set_tests_properties(vtkRenderingCoreCxx-TestTranslucentImageActorAlphaBlending PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTranslucentImageActorDepthPeeling "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTranslucentImageActorDepthPeeling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTranslucentImageActorDepthPeeling.png")
set_tests_properties(vtkRenderingCoreCxx-TestTranslucentImageActorDepthPeeling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTranslucentLUTDepthPeeling "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTranslucentLUTDepthPeeling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTranslucentLUTDepthPeeling.png")
set_tests_properties(vtkRenderingCoreCxx-TestTranslucentLUTDepthPeeling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTranslucentLUTTextureDepthPeeling "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTranslucentLUTTextureDepthPeeling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTranslucentLUTTextureDepthPeeling.png")
set_tests_properties(vtkRenderingCoreCxx-TestTranslucentLUTTextureDepthPeeling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTStripsColorsTCoords "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTStripsColorsTCoords" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTStripsColorsTCoords.png")
set_tests_properties(vtkRenderingCoreCxx-TestTStripsColorsTCoords PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTStripsNormalsColorsTCoords "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTStripsNormalsColorsTCoords" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTStripsNormalsColorsTCoords.png")
set_tests_properties(vtkRenderingCoreCxx-TestTStripsNormalsColorsTCoords PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTStripsNormalsTCoords "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTStripsNormalsTCoords" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTStripsNormalsTCoords.png")
set_tests_properties(vtkRenderingCoreCxx-TestTStripsNormalsTCoords PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestTStripsTCoords "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestTStripsTCoords" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestTStripsTCoords.png")
set_tests_properties(vtkRenderingCoreCxx-TestTStripsTCoords PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-TestWindowToImageFilter "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "TestWindowToImageFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/TestWindowToImageFilter.png")
set_tests_properties(vtkRenderingCoreCxx-TestWindowToImageFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-otherLookupTable "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "otherLookupTable" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-otherLookupTable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-otherLookupTableWithEnabling "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "otherLookupTableWithEnabling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingCoreCxx-otherLookupTableWithEnabling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-RenderNonFinite "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "RenderNonFinite" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/RenderNonFinite.png")
set_tests_properties(vtkRenderingCoreCxx-RenderNonFinite PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingCoreCxx-SurfacePlusEdges "/usr/src/Paraview5/bin/vtkRenderingCoreCxxTests" "SurfacePlusEdges" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Core/Testing/Data/Baseline/SurfacePlusEdges.png")
set_tests_properties(vtkRenderingCoreCxx-SurfacePlusEdges PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
