# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/Volume/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Rendering/Volume/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingVolumeCxx-ProjectedTetrahedraZoomIn "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "ProjectedTetrahedraZoomIn" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingVolumeCxx-ProjectedTetrahedraZoomIn PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestFinalColorWindowLevel "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestFinalColorWindowLevel" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestFinalColorWindowLevel.png")
set_tests_properties(vtkRenderingVolumeCxx-TestFinalColorWindowLevel PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestFixedPointRayCastLightComponents "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestFixedPointRayCastLightComponents" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestFixedPointRayCastLightComponents.png")
set_tests_properties(vtkRenderingVolumeCxx-TestFixedPointRayCastLightComponents PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastAdditive "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastAdditive" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastAdditive.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastAdditive PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastCompositeBinaryMask "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastCompositeBinaryMask" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastCompositeBinaryMask.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastCompositeBinaryMask PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastCompositeMaskBlend "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastCompositeMaskBlend" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastCompositeMaskBlend.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastCompositeMaskBlend PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastCompositeMask "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastCompositeMask" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastCompositeMask.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastCompositeMask PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastCompositeToMIP "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastCompositeToMIP" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastCompositeToMIP.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastCompositeToMIP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastCropping "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastCropping" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastCropping.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastCropping PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastDataTypesMinIP "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastDataTypesMinIP" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastDataTypesMinIP.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastDataTypesMinIP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastDataTypesMIP "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastDataTypesMIP" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastDataTypesMIP.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastDataTypesMIP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsComposite "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastFourComponentsComposite" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastFourComponentsComposite.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsComposite PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsCompositeStreaming "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastFourComponentsCompositeStreaming" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastFourComponentsCompositeStreaming.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsCompositeStreaming PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsMinIP "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastFourComponentsMinIP" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastFourComponentsMinIP.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsMinIP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsMIP "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastFourComponentsMIP" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastFourComponentsMIP.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastFourComponentsMIP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastMapperBenchmark "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastMapperBenchmark" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastMapperBenchmark.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastMapperBenchmark PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastMapperSampleDistance "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastMapperSampleDistance" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastMapperSampleDistance.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastMapperSampleDistance PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastMIPBinaryMask "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastMIPBinaryMask" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastMIPBinaryMask.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastMIPBinaryMask PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastMIPToComposite "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastMIPToComposite" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastMIPToComposite.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastMIPToComposite PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastNearestDataTypesMIP "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastNearestDataTypesMIP" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastNearestDataTypesMIP.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastNearestDataTypesMIP PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastPerspectiveParallel "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastPerspectiveParallel" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastPerspectiveParallel.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastPerspectiveParallel PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastVolumeUpdate "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastVolumeUpdate" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastVolumeUpdate.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastVolumeUpdate PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPUVolumeRayCastMapper "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPUVolumeRayCastMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPUVolumeRayCastMapper.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPUVolumeRayCastMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestMinIntensityRendering "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestMinIntensityRendering" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestMinIntensityRendering.png")
set_tests_properties(vtkRenderingVolumeCxx-TestMinIntensityRendering PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestProjectedTetrahedra "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestProjectedTetrahedra" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestProjectedTetrahedra.png")
set_tests_properties(vtkRenderingVolumeCxx-TestProjectedTetrahedra PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestSmartVolumeMapper "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestSmartVolumeMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestSmartVolumeMapper.png")
set_tests_properties(vtkRenderingVolumeCxx-TestSmartVolumeMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestSmartVolumeMapperWindowLevel "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestSmartVolumeMapperWindowLevel" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestSmartVolumeMapperWindowLevel.png")
set_tests_properties(vtkRenderingVolumeCxx-TestSmartVolumeMapperWindowLevel PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-HomogeneousRayIntegration "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "HomogeneousRayIntegration" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/HomogeneousRayIntegration.png")
set_tests_properties(vtkRenderingVolumeCxx-HomogeneousRayIntegration PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-LinearRayIntegration "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "LinearRayIntegration" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/LinearRayIntegration.png")
set_tests_properties(vtkRenderingVolumeCxx-LinearRayIntegration PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-PartialPreIntegration "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "PartialPreIntegration" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/PartialPreIntegration.png")
set_tests_properties(vtkRenderingVolumeCxx-PartialPreIntegration PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-PreIntegrationIncremental "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "PreIntegrationIncremental" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/PreIntegrationIncremental.png")
set_tests_properties(vtkRenderingVolumeCxx-PreIntegrationIncremental PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-PreIntegrationNonIncremental "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "PreIntegrationNonIncremental" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/PreIntegrationNonIncremental.png")
set_tests_properties(vtkRenderingVolumeCxx-PreIntegrationNonIncremental PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestGPURayCastCompositeShadeMask "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestGPURayCastCompositeShadeMask" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestGPURayCastCompositeShadeMask.png")
set_tests_properties(vtkRenderingVolumeCxx-TestGPURayCastCompositeShadeMask PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestHAVSVolumeMapper "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestHAVSVolumeMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestHAVSVolumeMapper.png")
set_tests_properties(vtkRenderingVolumeCxx-TestHAVSVolumeMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestProjectedHexahedra "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestProjectedHexahedra" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestProjectedHexahedra.png")
set_tests_properties(vtkRenderingVolumeCxx-TestProjectedHexahedra PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-ZsweepConcavities "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "ZsweepConcavities" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/ZsweepConcavities.png")
set_tests_properties(vtkRenderingVolumeCxx-ZsweepConcavities PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingVolumeCxx-TestProp3DFollower "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestProp3DFollower" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestProp3DFollower.png")
set_tests_properties(vtkRenderingVolumeCxx-TestProp3DFollower PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "" TIMEOUT "100")
add_test(vtkRenderingVolumeCxx-TestTM3DLightComponents "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "TestTM3DLightComponents" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/TestTM3DLightComponents.png")
set_tests_properties(vtkRenderingVolumeCxx-TestTM3DLightComponents PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "" TIMEOUT "120")
add_test(vtkRenderingVolumeCxx-volProt "/usr/src/Paraview5/bin/vtkRenderingVolumeCxxTests" "volProt" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Volume/Testing/Data/Baseline/volProt.png")
set_tests_properties(vtkRenderingVolumeCxx-volProt PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
