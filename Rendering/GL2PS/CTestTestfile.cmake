# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/GL2PS
# Build directory: /usr/src/Paraview5/VTK/Rendering/GL2PS
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingGL2PS-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Rendering/GL2PS" "VTKRENDERINGGL2PS_EXPORT")
set_tests_properties(vtkRenderingGL2PS-HeaderTest PROPERTIES  LABELS "")
