# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/Context2D
# Build directory: /usr/src/Paraview5/VTK/Rendering/Context2D
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingContext2D-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Rendering/Context2D" "VTKRENDERINGCONTEXT2D_EXPORT")
set_tests_properties(vtkRenderingContext2D-HeaderTest PROPERTIES  LABELS "")
