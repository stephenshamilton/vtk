# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/Label/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Rendering/Label/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingLabelCxx-TestDynamic2DLabelMapper "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestDynamic2DLabelMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestDynamic2DLabelMapper.png")
set_tests_properties(vtkRenderingLabelCxx-TestDynamic2DLabelMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingLabelCxx-TestLabelPlacer "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestLabelPlacer" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestLabelPlacer.png")
set_tests_properties(vtkRenderingLabelCxx-TestLabelPlacer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingLabelCxx-TestLabelPlacer2D "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestLabelPlacer2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestLabelPlacer2D.png")
set_tests_properties(vtkRenderingLabelCxx-TestLabelPlacer2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingLabelCxx-TestLabelPlacerCoincidentPoints "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestLabelPlacerCoincidentPoints" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestLabelPlacerCoincidentPoints.png")
set_tests_properties(vtkRenderingLabelCxx-TestLabelPlacerCoincidentPoints PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingLabelCxx-TestLabelPlacementMapper "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestLabelPlacementMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestLabelPlacementMapper.png")
set_tests_properties(vtkRenderingLabelCxx-TestLabelPlacementMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingLabelCxx-TestLabelPlacementMapper2D "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestLabelPlacementMapper2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestLabelPlacementMapper2D.png")
set_tests_properties(vtkRenderingLabelCxx-TestLabelPlacementMapper2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingLabelCxx-TestLabelPlacementMapperCoincidentPoints "/usr/src/Paraview5/bin/vtkRenderingLabelCxxTests" "TestLabelPlacementMapperCoincidentPoints" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Label/Testing/Data/Baseline/TestLabelPlacementMapperCoincidentPoints.png")
set_tests_properties(vtkRenderingLabelCxx-TestLabelPlacementMapperCoincidentPoints PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
