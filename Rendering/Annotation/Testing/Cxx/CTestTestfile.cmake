# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/Annotation/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Rendering/Annotation/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingAnnotationCxx-TestAxisActor3D "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestAxisActor3D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestAxisActor3D.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestAxisActor3D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestBarChartActor "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestBarChartActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestBarChartActor.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestBarChartActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCaptionActor2D "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCaptionActor2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCaptionActor2D.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCaptionActor2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestConvexHull2D "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestConvexHull2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingAnnotationCxx-TestConvexHull2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCornerAnnotation "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCornerAnnotation" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCornerAnnotation.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCornerAnnotation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestEmptyCornerAnnotation "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestEmptyCornerAnnotation" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestEmptyCornerAnnotation.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestEmptyCornerAnnotation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxes2DMode "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxes2DMode" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxes2DMode.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxes2DMode PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxes3 "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxes3" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxes3.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxes3 PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithGridLines "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithGridLines" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithGridLines.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithGridLines PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithXInnerGrids "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithXInnerGrids" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithXInnerGrids.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithXInnerGrids PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithXInnerPolys "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithXInnerPolys" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithXInnerPolys.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithXInnerPolys PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithXLines "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithXLines" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithXLines.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithXLines PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithYInnerGrids "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithYInnerGrids" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithYInnerGrids.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithYInnerGrids PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithYInnerPolys "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithYInnerPolys" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithYInnerPolys.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithYInnerPolys PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithYLines "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithYLines" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithYLines.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithYLines PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithZInnerGrids "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithZInnerGrids" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithZInnerGrids.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithZInnerGrids PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithZInnerPolys "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithZInnerPolys" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithZInnerPolys.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithZInnerPolys PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesWithZLines "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesWithZLines" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesWithZLines.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesWithZLines PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestLegendBoxActor "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestLegendBoxActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestLegendBoxActor.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestLegendBoxActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestLegendScaleActor "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestLegendScaleActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestLegendScaleActor.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestLegendScaleActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestPieChartActor "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestPieChartActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestPieChartActor.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestPieChartActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestPolarAxes "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestPolarAxes" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestPolarAxes.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestPolarAxes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestScalarBar "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestScalarBar" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestScalarBar.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestScalarBar PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestSpiderPlotActor "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestSpiderPlotActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestSpiderPlotActor.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestSpiderPlotActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesOrientedBoundingBox "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesOrientedBoundingBox" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesOrientedBoundingBox.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesOrientedBoundingBox PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesIntersectionPoint "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesIntersectionPoint" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesIntersectionPoint.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesIntersectionPoint PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesInnerGridAll "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesInnerGridAll" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesInnerGridAll.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesInnerGridAll PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesInnerGridClosest "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesInnerGridClosest" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesInnerGridClosest.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesInnerGridClosest PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesInnerGridFurthest "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesInnerGridFurthest" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesInnerGridFurthest.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesInnerGridFurthest PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesSticky "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesSticky" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesSticky.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesSticky PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestCubeAxesStickyCentered "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestCubeAxesStickyCentered" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestCubeAxesStickyCentered.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestCubeAxesStickyCentered PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingAnnotationCxx-TestXYPlotActor "/usr/src/Paraview5/bin/vtkRenderingAnnotationCxxTests" "TestXYPlotActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/Annotation/Testing/Data/Baseline/TestXYPlotActor.png")
set_tests_properties(vtkRenderingAnnotationCxx-TestXYPlotActor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
