# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Rendering/FreeType/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Rendering/FreeType/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkRenderingFreeTypeCxx-TestFTStringToPath "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestFTStringToPath" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestFTStringToPath.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestFTStringToPath PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestFreeTypeTextMapperNoMath "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestFreeTypeTextMapperNoMath" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Fonts/DejaVuSans.ttf" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestFreeTypeTextMapperNoMath.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestFreeTypeTextMapperNoMath PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestFreeTypeTools "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestFreeTypeTools" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkRenderingFreeTypeCxx-TestFreeTypeTools PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestMathTextFreeTypeTextRendererNoMath "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestMathTextFreeTypeTextRendererNoMath" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Fonts/DejaVuSans.ttf" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestMathTextFreeTypeTextRendererNoMath.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestMathTextFreeTypeTextRendererNoMath PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActor "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActor.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActor PROPERTIES  FAIL_REGULAR_EXPRESSION "ERROR:" LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActor3D "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActor3D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActor3D.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActor3D PROPERTIES  FAIL_REGULAR_EXPRESSION "ERROR:" LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActorAlphaBlending "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActorAlphaBlending" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActorAlphaBlending.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActorAlphaBlending PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActorDepthPeeling "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActorDepthPeeling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActorDepthPeeling.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActorDepthPeeling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActor3DAlphaBlending "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActor3DAlphaBlending" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActor3DAlphaBlending.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActor3DAlphaBlending PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActor3DDepthPeeling "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActor3DDepthPeeling" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActor3DDepthPeeling.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActor3DDepthPeeling PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextActorScaleModeProp "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextActorScaleModeProp" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextActorScaleModeProp.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextActorScaleModeProp PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkRenderingFreeTypeCxx-TestTextMapper "/usr/src/Paraview5/bin/vtkRenderingFreeTypeCxxTests" "TestTextMapper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Rendering/FreeType/Testing/Data/Baseline/TestTextMapper.png")
set_tests_properties(vtkRenderingFreeTypeCxx-TestTextMapper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
