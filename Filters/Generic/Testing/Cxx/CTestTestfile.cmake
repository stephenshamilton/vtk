# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Generic/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Generic/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersGenericCxx-TestGenericStreamTracer "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericStreamTracer" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericStreamTracer.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericStreamTracer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericClip "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericClip" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericClip.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericClip PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericContourFilter "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericContourFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericContourFilter.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericContourFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericCutter "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericCutter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericCutter.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericCutter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericDataSetTessellator "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericDataSetTessellator" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericDataSetTessellator.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericDataSetTessellator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericGeometryFilter "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericGeometryFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericGeometryFilter.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericGeometryFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericGlyph3DFilter "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericGlyph3DFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericGlyph3DFilter.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericGlyph3DFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-TestGenericProbeFilter "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "TestGenericProbeFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Generic/Testing/Data/Baseline/TestGenericProbeFilter.png")
set_tests_properties(vtkFiltersGenericCxx-TestGenericProbeFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGenericCxx-otherCreation "/usr/src/Paraview5/bin/vtkFiltersGenericCxxTests" "otherCreation" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersGenericCxx-otherCreation PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
