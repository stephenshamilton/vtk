# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Core/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Core/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersCoreCxx-TestAppendFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestAppendFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestAppendFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestAppendPolyData "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestAppendPolyData" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestAppendPolyData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestAppendSelection "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestAppendSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestAppendSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestArrayCalculator "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestArrayCalculator" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestArrayCalculator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestAssignAttribute "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestAssignAttribute" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestAssignAttribute PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestCellDataToPointData "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestCellDataToPointData" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestCellDataToPointData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestCenterOfMass "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestCenterOfMass" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestCenterOfMass PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestCleanPolyData "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestCleanPolyData" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestCleanPolyData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestClipPolyData "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestClipPolyData" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestClipPolyData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestConnectivityFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestConnectivityFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestConnectivityFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestCutter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestCutter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestCutter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestDecimatePolylineFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestDecimatePolylineFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Core/Testing/Data/Baseline/TestDecimatePolylineFilter.png")
set_tests_properties(vtkFiltersCoreCxx-TestDecimatePolylineFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestDecimatePro "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestDecimatePro" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestDecimatePro PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestDelaunay2D "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestDelaunay2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Core/Testing/Data/Baseline/TestDelaunay2D.png")
set_tests_properties(vtkFiltersCoreCxx-TestDelaunay2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestDelaunay2DFindTriangle "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestDelaunay2DFindTriangle" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestDelaunay2DFindTriangle PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestDelaunay2DMeshes "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestDelaunay2DMeshes" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestDelaunay2DMeshes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestDelaunay3D "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestDelaunay3D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestDelaunay3D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestExecutionTimer "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestExecutionTimer" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestExecutionTimer PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestFeatureEdges "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestFeatureEdges" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestFeatureEdges PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestFlyingEdges "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestFlyingEdges" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Core/Testing/Data/Baseline/TestFlyingEdges.png")
set_tests_properties(vtkFiltersCoreCxx-TestFlyingEdges PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestGlyph3D "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestGlyph3D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Core/Testing/Data/Baseline/TestGlyph3D.png")
set_tests_properties(vtkFiltersCoreCxx-TestGlyph3D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestHedgeHog "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestHedgeHog" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestHedgeHog PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestImplicitPolyDataDistance "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestImplicitPolyDataDistance" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Core/Testing/Data/Baseline/TestImplicitPolyDataDistance.png")
set_tests_properties(vtkFiltersCoreCxx-TestImplicitPolyDataDistance PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestMaskPoints "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestMaskPoints" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestMaskPoints PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestNamedComponents "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestNamedComponents" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestNamedComponents PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestPolyDataConnectivityFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestPolyDataConnectivityFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestPolyDataConnectivityFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestProbeFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestProbeFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestProbeFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestProbeFilterImageInput "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestProbeFilterImageInput" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Core/Testing/Data/Baseline/TestProbeFilterImageInput.png")
set_tests_properties(vtkFiltersCoreCxx-TestProbeFilterImageInput PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestSmoothPolyDataFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestSmoothPolyDataFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestSmoothPolyDataFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestSMPPipelineContour "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestSMPPipelineContour" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestSMPPipelineContour PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestStripper "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestStripper" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestStripper PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestStructuredGridAppend "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestStructuredGridAppend" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestStructuredGridAppend PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestThreshold "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestThreshold" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestThreshold PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestThresholdPoints "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestThresholdPoints" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestThresholdPoints PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestTransposeTable "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestTransposeTable" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestTransposeTable PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-TestTubeFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "TestTubeFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-TestTubeFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-UnitTestMaskPoints "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "UnitTestMaskPoints" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-UnitTestMaskPoints PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersCoreCxx-UnitTestMergeFilter "/usr/src/Paraview5/bin/vtkFiltersCoreCxxTests" "UnitTestMergeFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersCoreCxx-UnitTestMergeFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
