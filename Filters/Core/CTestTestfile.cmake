# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Core
# Build directory: /usr/src/Paraview5/VTK/Filters/Core
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersCore-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Filters/Core" "VTKFILTERSCORE_EXPORT")
set_tests_properties(vtkFiltersCore-HeaderTest PROPERTIES  LABELS "")
