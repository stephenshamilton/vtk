# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Extraction/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Extraction/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersExtractionCxx-TestConvertSelection "/usr/src/Paraview5/bin/vtkFiltersExtractionCxxTests" "TestConvertSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersExtractionCxx-TestConvertSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersExtractionCxx-TestExtractSelection "/usr/src/Paraview5/bin/vtkFiltersExtractionCxxTests" "TestExtractSelection" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Extraction/Testing/Data/Baseline/TestExtractSelection.png")
set_tests_properties(vtkFiltersExtractionCxx-TestExtractSelection PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersExtractionCxx-TestExtraction "/usr/src/Paraview5/bin/vtkFiltersExtractionCxxTests" "TestExtraction" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Extraction/Testing/Data/Baseline/TestExtraction.png")
set_tests_properties(vtkFiltersExtractionCxx-TestExtraction PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersExtractionCxx-TestExtractRectilinearGrid "/usr/src/Paraview5/bin/vtkFiltersExtractionCxxTests" "TestExtractRectilinearGrid" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersExtractionCxx-TestExtractRectilinearGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
