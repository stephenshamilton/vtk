# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/HyperTree/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/HyperTree/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersHyperTreeCxx-TestClipHyperOctree "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestClipHyperOctree" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestClipHyperOctree.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestClipHyperOctree PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperOctreeContourFilter "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperOctreeContourFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperOctreeContourFilter.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperOctreeContourFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperOctreeCutter "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperOctreeCutter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperOctreeCutter.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperOctreeCutter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperOctreeDual "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperOctreeDual" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperOctreeDual.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperOctreeDual PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperOctreeSurfaceFilter "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperOctreeSurfaceFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperOctreeSurfaceFilter.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperOctreeSurfaceFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperOctreeToUniformGrid "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperOctreeToUniformGrid" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperOctreeToUniformGrid.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperOctreeToUniformGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2D "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridBinary2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridBinary2D.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2DIJK "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridBinary2DIJK" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridBinary2DIJK.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2DIJK PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2DMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridBinary2DMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridBinary2DMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2DMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2DMaterialIJK "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridBinary2DMaterialIJK" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridBinary2DMaterialIJK.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinary2DMaterialIJK PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinaryEllipseMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridBinaryEllipseMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridBinaryEllipseMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinaryEllipseMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinaryHyperbolicParaboloidMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridBinaryHyperbolicParaboloidMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridBinaryHyperbolicParaboloidMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridBinaryHyperbolicParaboloidMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2D "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary2D.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary2DMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary2DMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DMaterialBits "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary2DMaterialBits" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary2DMaterialBits.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DMaterialBits PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DFullMaterialBits "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary2DFullMaterialBits" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary2DFullMaterialBits.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DFullMaterialBits PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DBiMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary2DBiMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary2DBiMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary2DBiMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DAxisCut "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DAxisCut" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DAxisCut.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DAxisCut PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DAxisCutMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DAxisCutMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DAxisCutMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DAxisCutMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DContour "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DContour" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DContour.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DContour PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DContourMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DContourMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DContourMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DContourMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DClip "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DClip" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DClip.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DClip PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DCut "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DCut" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DCut.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DCut PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometry "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DGeometry" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DGeometry.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometry PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometryMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DGeometryMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DGeometryMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometryMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometryMaterialBits "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DGeometryMaterialBits" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DGeometryMaterialBits.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometryMaterialBits PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometryLargeMaterialBits "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DGeometryLargeMaterialBits" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DGeometryLargeMaterialBits.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DGeometryLargeMaterialBits PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DUnstructured "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DUnstructured" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DUnstructured.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DUnstructured PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DUnstructuredMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernary3DUnstructuredMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernary3DUnstructuredMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernary3DUnstructuredMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernaryHyperbola "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernaryHyperbola" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernaryHyperbola.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernaryHyperbola PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernarySphereMaterial "/usr/src/Paraview5/bin/vtkFiltersHyperTreeCxxTests" "TestHyperTreeGridTernarySphereMaterial" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/HyperTree/Testing/Data/Baseline/TestHyperTreeGridTernarySphereMaterial.png")
set_tests_properties(vtkFiltersHyperTreeCxx-TestHyperTreeGridTernarySphereMaterial PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
