# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Programmable/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Programmable/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersProgrammableCxx-TestProgrammableGlyph "/usr/src/Paraview5/bin/vtkFiltersProgrammableCxxTests" "TestProgrammableGlyph" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Programmable/Testing/Data/Baseline/TestProgrammableGlyph.png")
set_tests_properties(vtkFiltersProgrammableCxx-TestProgrammableGlyph PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
