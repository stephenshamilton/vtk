# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Sources/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Sources/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersSourcesCxx-TestArcSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestArcSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestArcSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestConeSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestConeSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestConeSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestCubeSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestCubeSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestCubeSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestCylinderSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestCylinderSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestCylinderSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestDiskSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestDiskSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestDiskSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestEllipticalButtonSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestEllipticalButtonSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestEllipticalButtonSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestFrustumSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestFrustumSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestFrustumSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestGlyphSource2D "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestGlyphSource2D" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestGlyphSource2D PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestLineSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestLineSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestLineSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestMultiBlock "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestMultiBlock" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Sources/Testing/Data/Baseline/TestMultiBlock.png")
set_tests_properties(vtkFiltersSourcesCxx-TestMultiBlock PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestOutlineCornerSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestOutlineCornerSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestOutlineCornerSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestOutlineSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestOutlineSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestOutlineSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestParametricFunctionSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestParametricFunctionSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestParametricFunctionSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestPlaneSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestPlaneSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestPlaneSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestPlatonicSolidSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestPlatonicSolidSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestPlatonicSolidSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestPointSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestPointSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestPointSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestPolyLineSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestPolyLineSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestPolyLineSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestRectangularButtonSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestRectangularButtonSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestRectangularButtonSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestRegularPolygonSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestRegularPolygonSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestRegularPolygonSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestSphereSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestSphereSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestSphereSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestSuperquadricSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestSuperquadricSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestSuperquadricSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestTessellatedBoxSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestTessellatedBoxSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestTessellatedBoxSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestTextSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestTextSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestTextSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestTexturedSphereSource "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestTexturedSphereSource" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersSourcesCxx-TestTexturedSphereSource PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersSourcesCxx-TestGlyphSource2DResolution "/usr/src/Paraview5/bin/vtkFiltersSourcesCxxTests" "TestGlyphSource2DResolution" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Sources/Testing/Data/Baseline/TestGlyphSource2DResolution.png")
set_tests_properties(vtkFiltersSourcesCxx-TestGlyphSource2DResolution PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
