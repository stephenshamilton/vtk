# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Texture
# Build directory: /usr/src/Paraview5/VTK/Filters/Texture
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersTexture-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Filters/Texture" "VTKFILTERSTEXTURE_EXPORT")
set_tests_properties(vtkFiltersTexture-HeaderTest PROPERTIES  LABELS "")
