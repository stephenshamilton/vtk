# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/AMR/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/AMR/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersAMRCxx-TestAMRGhostLayerStripping "/usr/src/Paraview5/bin/vtkFiltersAMRCxxTests" "TestAMRGhostLayerStripping" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkFiltersAMRCxx-TestAMRGhostLayerStripping PROPERTIES  FAIL_REGULAR_EXPRESSION "Error" LABELS "")
add_test(vtkFiltersAMRCxx-TestAMRBlanking "/usr/src/Paraview5/bin/vtkFiltersAMRCxxTests" "TestAMRBlanking" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkFiltersAMRCxx-TestAMRBlanking PROPERTIES  FAIL_REGULAR_EXPRESSION "Error" LABELS "")
add_test(vtkFiltersAMRCxx-TestAMRIterator "/usr/src/Paraview5/bin/vtkFiltersAMRCxxTests" "TestAMRIterator" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkFiltersAMRCxx-TestAMRIterator PROPERTIES  FAIL_REGULAR_EXPRESSION "Error" LABELS "")
add_test(vtkFiltersAMRCxx-TestImageToAMR "/usr/src/Paraview5/bin/vtkFiltersAMRCxxTests" "TestImageToAMR" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkFiltersAMRCxx-TestImageToAMR PROPERTIES  FAIL_REGULAR_EXPRESSION "Error" LABELS "")
