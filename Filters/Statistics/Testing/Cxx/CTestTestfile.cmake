# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Statistics/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Statistics/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersStatisticsCxx-TestAutoCorrelativeStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestAutoCorrelativeStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestAutoCorrelativeStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestComputeQuartiles "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestComputeQuartiles")
set_tests_properties(vtkFiltersStatisticsCxx-TestComputeQuartiles PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestContingencyStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestContingencyStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestContingencyStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestCorrelativeStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestCorrelativeStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestCorrelativeStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestDescriptiveStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestDescriptiveStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestDescriptiveStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestHighestDensityRegionsStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestHighestDensityRegionsStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestHighestDensityRegionsStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestExtractFunctionalBagPlot "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestExtractFunctionalBagPlot")
set_tests_properties(vtkFiltersStatisticsCxx-TestExtractFunctionalBagPlot PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestKMeansStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestKMeansStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestKMeansStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestMultiCorrelativeStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestMultiCorrelativeStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestMultiCorrelativeStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestOrderStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestOrderStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestOrderStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersStatisticsCxx-TestPCAStatistics "/usr/src/Paraview5/bin/vtkFiltersStatisticsCxxTests" "TestPCAStatistics")
set_tests_properties(vtkFiltersStatisticsCxx-TestPCAStatistics PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
