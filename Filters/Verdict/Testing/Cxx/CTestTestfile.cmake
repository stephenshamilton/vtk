# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Verdict/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Verdict/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersVerdictCxx-MeshQuality "/usr/src/Paraview5/bin/vtkFiltersVerdictCxxTests" "MeshQuality" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersVerdictCxx-MeshQuality PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
