# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Geometry/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Geometry/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersGeometryCxx-TestExtractSurfaceNonLinearSubdivision "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestExtractSurfaceNonLinearSubdivision" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Geometry/Testing/Data/Baseline/TestExtractSurfaceNonLinearSubdivision.png")
set_tests_properties(vtkFiltersGeometryCxx-TestExtractSurfaceNonLinearSubdivision PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestDataSetSurfaceFieldData "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestDataSetSurfaceFieldData" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersGeometryCxx-TestDataSetSurfaceFieldData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestImageDataToUniformGrid "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestImageDataToUniformGrid" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersGeometryCxx-TestImageDataToUniformGrid PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestProjectSphereFilter "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestProjectSphereFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersGeometryCxx-TestProjectSphereFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestStructuredAMRNeighbor "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestStructuredAMRNeighbor" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersGeometryCxx-TestStructuredAMRNeighbor PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestUniformGridGhostDataGenerator "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestUniformGridGhostDataGenerator" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkFiltersGeometryCxx-TestUniformGridGhostDataGenerator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestUnstructuredGridGeometryFilter "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestUnstructuredGridGeometryFilter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Geometry/Testing/Data/Baseline/TestUnstructuredGridGeometryFilter.png")
set_tests_properties(vtkFiltersGeometryCxx-TestUnstructuredGridGeometryFilter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestGeometryFilterCellData "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestGeometryFilterCellData")
set_tests_properties(vtkFiltersGeometryCxx-TestGeometryFilterCellData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestStructuredAMRGridConnectivity "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestStructuredAMRGridConnectivity")
set_tests_properties(vtkFiltersGeometryCxx-TestStructuredAMRGridConnectivity PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestStructuredGridConnectivity "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestStructuredGridConnectivity")
set_tests_properties(vtkFiltersGeometryCxx-TestStructuredGridConnectivity PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersGeometryCxx-TestStructuredGridGhostDataGenerator "/usr/src/Paraview5/bin/vtkFiltersGeometryCxxTests" "TestStructuredGridGhostDataGenerator")
set_tests_properties(vtkFiltersGeometryCxx-TestStructuredGridGhostDataGenerator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
