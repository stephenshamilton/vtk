# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Modeling/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/Filters/Modeling/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersModelingCxx-TestButterflyScalars "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestButterflyScalars" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestButterflyScalars.png")
set_tests_properties(vtkFiltersModelingCxx-TestButterflyScalars PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersModelingCxx-TestNamedColorsIntegration "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestNamedColorsIntegration" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestNamedColorsIntegration.png")
set_tests_properties(vtkFiltersModelingCxx-TestNamedColorsIntegration PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersModelingCxx-TestPolyDataPointSampler "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestPolyDataPointSampler" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestPolyDataPointSampler.png")
set_tests_properties(vtkFiltersModelingCxx-TestPolyDataPointSampler PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersModelingCxx-TestQuadRotationalExtrusion "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestQuadRotationalExtrusion" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestQuadRotationalExtrusion.png")
set_tests_properties(vtkFiltersModelingCxx-TestQuadRotationalExtrusion PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersModelingCxx-TestQuadRotationalExtrusionMultiBlock "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestQuadRotationalExtrusionMultiBlock" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestQuadRotationalExtrusionMultiBlock.png")
set_tests_properties(vtkFiltersModelingCxx-TestQuadRotationalExtrusionMultiBlock PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersModelingCxx-TestRotationalExtrusion "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestRotationalExtrusion" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestRotationalExtrusion.png")
set_tests_properties(vtkFiltersModelingCxx-TestRotationalExtrusion PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkFiltersModelingCxx-TestSelectEnclosedPoints "/usr/src/Paraview5/bin/vtkFiltersModelingCxxTests" "TestSelectEnclosedPoints" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/Filters/Modeling/Testing/Data/Baseline/TestSelectEnclosedPoints.png")
set_tests_properties(vtkFiltersModelingCxx-TestSelectEnclosedPoints PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
