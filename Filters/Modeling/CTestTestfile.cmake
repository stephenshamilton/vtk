# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/Filters/Modeling
# Build directory: /usr/src/Paraview5/VTK/Filters/Modeling
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkFiltersModeling-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/Filters/Modeling" "VTKFILTERSMODELING_EXPORT")
set_tests_properties(vtkFiltersModeling-HeaderTest PROPERTIES  LABELS "")
