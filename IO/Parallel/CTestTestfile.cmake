# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Parallel
# Build directory: /usr/src/Paraview5/VTK/IO/Parallel
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOParallel-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/IO/Parallel" "VTKIOPARALLEL_EXPORT")
set_tests_properties(vtkIOParallel-HeaderTest PROPERTIES  LABELS "")
