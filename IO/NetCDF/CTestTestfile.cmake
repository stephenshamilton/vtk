# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/NetCDF
# Build directory: /usr/src/Paraview5/VTK/IO/NetCDF
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIONetCDF-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/IO/NetCDF" "VTKIONETCDF_EXPORT")
set_tests_properties(vtkIONetCDF-HeaderTest PROPERTIES  LABELS "")
