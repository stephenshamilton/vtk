# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/ParallelExodus/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/ParallelExodus/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOParallelExodusCxx-TestExodusImplicitArrays "/usr/src/Paraview5/bin/vtkIOParallelExodusCxxTests" "TestExodusImplicitArrays" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOParallelExodusCxx-TestExodusImplicitArrays PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
