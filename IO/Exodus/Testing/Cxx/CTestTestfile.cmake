# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Exodus/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Exodus/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOExodusCxx-TestExodusAttributes "/usr/src/Paraview5/bin/vtkIOExodusCxxTests" "TestExodusAttributes" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOExodusCxx-TestExodusAttributes PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOExodusCxx-TestExodusSideSets "/usr/src/Paraview5/bin/vtkIOExodusCxxTests" "TestExodusSideSets" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOExodusCxx-TestExodusSideSets PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOExodusCxx-TestInSituExodus "/usr/src/Paraview5/bin/vtkIOExodusCxxTests" "TestInSituExodus" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOExodusCxx-TestInSituExodus PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
