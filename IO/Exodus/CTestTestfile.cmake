# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Exodus
# Build directory: /usr/src/Paraview5/VTK/IO/Exodus
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOExodus-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/IO/Exodus" "VTKIOEXODUS_EXPORT")
set_tests_properties(vtkIOExodus-HeaderTest PROPERTIES  LABELS "")
