# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Geometry/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Geometry/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOGeometryCxx-TestDataObjectIO "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestDataObjectIO" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-TestDataObjectIO PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestIncrementalOctreePointLocator "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestIncrementalOctreePointLocator" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-TestIncrementalOctreePointLocator PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-UnstructuredGridCellGradients "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "UnstructuredGridCellGradients" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/UnstructuredGridCellGradients.png")
set_tests_properties(vtkIOGeometryCxx-UnstructuredGridCellGradients PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-UnstructuredGridFastGradients "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "UnstructuredGridFastGradients" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/UnstructuredGridFastGradients.png")
set_tests_properties(vtkIOGeometryCxx-UnstructuredGridFastGradients PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-UnstructuredGridGradients "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "UnstructuredGridGradients" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/UnstructuredGridGradients.png")
set_tests_properties(vtkIOGeometryCxx-UnstructuredGridGradients PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestOBJReaderRelative "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestOBJReaderRelative" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-TestOBJReaderRelative PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestOBJReaderNormalsTCoords "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestOBJReaderNormalsTCoords" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-TestOBJReaderNormalsTCoords PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestOpenFOAMReader "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestOpenFOAMReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestOpenFOAMReader.png")
set_tests_properties(vtkIOGeometryCxx-TestOpenFOAMReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestProStarReader "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestProStarReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestProStarReader.png")
set_tests_properties(vtkIOGeometryCxx-TestProStarReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestTecplotReader "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestTecplotReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestTecplotReader.png")
set_tests_properties(vtkIOGeometryCxx-TestTecplotReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestAMRReadWrite "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestAMRReadWrite" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-TestAMRReadWrite PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestSimplePointsReaderWriter "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestSimplePointsReaderWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-TestSimplePointsReaderWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-UnitTestSTLWriter "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "UnitTestSTLWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOGeometryCxx-UnitTestSTLWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestSTLReaderSinglePatch "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestSTLReader" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/42400-IDGH.stl" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestSTLReaderSinglePatch.png")
set_tests_properties(vtkIOGeometryCxx-TestSTLReaderSinglePatch PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestSTLReaderMultiplePatches "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestSTLReader" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/multiple_patches.stl" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestSTLReaderMultiplePatches.png")
set_tests_properties(vtkIOGeometryCxx-TestSTLReaderMultiplePatches PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestPTSReader "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestPTSReader" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/samplePTS.pts" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestPTSReader.png")
set_tests_properties(vtkIOGeometryCxx-TestPTSReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestAVSucdReaderContiguousASCII "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestAVSucdReader" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/cellsnd.ascii.inp" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestAVSucdReaderContiguousASCII.png")
set_tests_properties(vtkIOGeometryCxx-TestAVSucdReaderContiguousASCII PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestAVSucdReaderNonContiguousASCII "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestAVSucdReader" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/cellsnd.noncontiguous.ascii.inp" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestAVSucdReaderNonContiguousASCII.png")
set_tests_properties(vtkIOGeometryCxx-TestAVSucdReaderNonContiguousASCII PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOGeometryCxx-TestAVSucdReaderBinary "/usr/src/Paraview5/bin/vtkIOGeometryCxxTests" "TestAVSucdReader" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/cellsnd.bin.inp" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Geometry/Testing/Data/Baseline/TestAVSucdReaderBinary.png")
set_tests_properties(vtkIOGeometryCxx-TestAVSucdReaderBinary PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
