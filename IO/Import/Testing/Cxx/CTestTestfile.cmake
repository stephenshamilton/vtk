# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Import/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Import/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOImportCxx-TestOBJImporter "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestOBJImporter" "-D" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_1.obj" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_1.obj.mtl" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/flare.jpg" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/map1024.png" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOImportCxx-TestOBJImporter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOImportCxx-TestVRMLNormals "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestVRMLNormals" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Baseline/TestVRMLNormals.png")
set_tests_properties(vtkIOImportCxx-TestVRMLNormals PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOImportCxx-TestVRMLImporter "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestVRMLImporter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOImportCxx-TestVRMLImporter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOImportCxx-OBJImport-MixedOrder1 "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestOBJImporter" "-D" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_2.obj" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_2.obj.mtl" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/flare.jpg" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/map1024.png" "-D" " /usr/src/Paraview5/VTK/Testing/Temporary ")
add_test(vtkIOImportCxx-OBJImport-NoMTL "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestOBJImporter" "-D" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_3_no_mtl.obj" "-D" " /usr/src/Paraview5/VTK/Testing/Temporary ")
add_test(vtkIOImportCxx-OBJImport-SolidAndTextured "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestOBJImporter" "-D" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/cube-scene.obj" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/cube-scene.mtl" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/noise.png" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/noise.png" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Baseline/OBJImport-SolidAndTextured.png" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
add_test(vtkIOImportCxx-OBJImport-MTLwithoutTextureFile "/usr/src/Paraview5/bin/vtkIOImportCxxTests" "TestOBJImporter" "-D" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_4_mtl_no_texture.obj" "/usr/src/Paraview5/ExternalData/VTK/IO/Import/Testing/Data/Input/boxes_4_mtl_no_texture.obj.mtl" "-D" " /usr/src/Paraview5/VTK/Testing/Temporary ")
