# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Legacy/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Legacy/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOLegacyCxx-TestLegacyCompositeDataReaderWriter "/usr/src/Paraview5/bin/vtkIOLegacyCxxTests" "TestLegacyCompositeDataReaderWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOLegacyCxx-TestLegacyCompositeDataReaderWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOLegacyCxx-TestLegacyGhostCellsImport "/usr/src/Paraview5/bin/vtkIOLegacyCxxTests" "TestLegacyGhostCellsImport" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/Legacy/Testing/Data/Baseline/TestLegacyGhostCellsImport.png")
set_tests_properties(vtkIOLegacyCxx-TestLegacyGhostCellsImport PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
