# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/PLY/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/PLY/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOPLYCxx-TestPLYReader "/usr/src/Paraview5/bin/vtkIOPLYCxxTests" "TestPLYReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/PLY/Testing/Data/Baseline/TestPLYReader.png")
set_tests_properties(vtkIOPLYCxx-TestPLYReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOPLYCxx-TestPLYReaderTextureUV "/usr/src/Paraview5/bin/vtkIOPLYCxxTests" "TestPLYReaderTextureUV" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/PLY/Testing/Data/Baseline/TestPLYReaderTextureUV.png")
set_tests_properties(vtkIOPLYCxx-TestPLYReaderTextureUV PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOPLYCxx-TestPLYWriter "/usr/src/Paraview5/bin/vtkIOPLYCxxTests" "TestPLYWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOPLYCxx-TestPLYWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
