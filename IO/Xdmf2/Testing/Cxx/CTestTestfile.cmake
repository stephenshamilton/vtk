# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Xdmf2/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Xdmf2/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOXdmf2Cxx-TestTemporalXdmfReaderWriter "/usr/src/Paraview5/bin/vtkIOXdmf2CxxTests" "TestTemporalXdmfReaderWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOXdmf2Cxx-TestTemporalXdmfReaderWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXdmf2Cxx-XdmfTestVTKIO "/usr/src/Paraview5/bin/vtkIOXdmf2CxxTests" "XdmfTestVTKIO" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOXdmf2Cxx-XdmfTestVTKIO PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
