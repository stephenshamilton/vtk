# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Xdmf2
# Build directory: /usr/src/Paraview5/VTK/IO/Xdmf2
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOXdmf2-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/IO/Xdmf2" "VTKIOXDMF2_EXPORT")
set_tests_properties(vtkIOXdmf2-HeaderTest PROPERTIES  LABELS "")
