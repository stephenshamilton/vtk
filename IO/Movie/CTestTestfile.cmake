# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Movie
# Build directory: /usr/src/Paraview5/VTK/IO/Movie
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOMovie-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/IO/Movie" "VTKIOMOVIE_EXPORT")
set_tests_properties(vtkIOMovie-HeaderTest PROPERTIES  LABELS "")
