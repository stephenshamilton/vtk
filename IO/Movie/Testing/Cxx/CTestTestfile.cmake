# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Movie/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Movie/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOMovieCxx-TestOggTheoraWriter "/usr/src/Paraview5/bin/vtkIOMovieCxxTests" "TestOggTheoraWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOMovieCxx-TestOggTheoraWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
