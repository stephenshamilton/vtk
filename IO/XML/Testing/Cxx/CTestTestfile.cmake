# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/XML/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/XML/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOXMLCxx-TestAMRXMLIO "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestAMRXMLIO" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOXMLCxx-TestAMRXMLIO PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestHyperOctreeIO "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestHyperOctreeIO" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/XML/Testing/Data/Baseline/TestHyperOctreeIO.png")
set_tests_properties(vtkIOXMLCxx-TestHyperOctreeIO PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLGhostCellsImport "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLGhostCellsImport" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/XML/Testing/Data/Baseline/TestXMLGhostCellsImport.png")
set_tests_properties(vtkIOXMLCxx-TestXMLGhostCellsImport PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLHierarchicalBoxDataFileConverter "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLHierarchicalBoxDataFileConverter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOXMLCxx-TestXMLHierarchicalBoxDataFileConverter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLUnstructuredGridReader "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLUnstructuredGridReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary" "-V" "/usr/src/Paraview5/ExternalData/VTK/IO/XML/Testing/Data/Baseline/TestXMLUnstructuredGridReader.png")
set_tests_properties(vtkIOXMLCxx-TestXMLUnstructuredGridReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXML "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXML" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/sample.xml")
set_tests_properties(vtkIOXMLCxx-TestXML PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLToString "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLToString")
set_tests_properties(vtkIOXMLCxx-TestXMLToString PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestDataObjectXMLIO "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestDataObjectXMLIO" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOXMLCxx-TestDataObjectXMLIO PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLReaderBadImageData "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLReaderBadData" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/badImageData.xml" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOXMLCxx-TestXMLReaderBadImageData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLReaderBadPolyData "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLReaderBadData" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/badPolyData.xml" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOXMLCxx-TestXMLReaderBadPolyData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLReaderBadRectilinearGridData "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLReaderBadData" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/badRectilinearGridData.xml" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOXMLCxx-TestXMLReaderBadRectilinearGridData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLReaderBadUnstucturedGridData "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLReaderBadData" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/badUnstructuredGridData.xml" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOXMLCxx-TestXMLReaderBadUnstucturedGridData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOXMLCxx-TestXMLReaderBadUniformGridData "/usr/src/Paraview5/bin/vtkIOXMLCxxTests" "TestXMLReaderBadData" "/usr/src/Paraview5/ExternalData/VTK/Testing/Data/badUniformGridData.xml" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOXMLCxx-TestXMLReaderBadUniformGridData PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
