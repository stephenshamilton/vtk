# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/Core/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/Core/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOCoreCxx-TestArrayDataWriter "/usr/src/Paraview5/bin/vtkIOCoreCxxTests" "TestArrayDataWriter" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOCoreCxx-TestArrayDataWriter PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOCoreCxx-TestArrayDenormalized "/usr/src/Paraview5/bin/vtkIOCoreCxxTests" "TestArrayDenormalized" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOCoreCxx-TestArrayDenormalized PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOCoreCxx-TestArraySerialization "/usr/src/Paraview5/bin/vtkIOCoreCxxTests" "TestArraySerialization" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOCoreCxx-TestArraySerialization PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
add_test(vtkIOCoreCxx-TestCompress "/usr/src/Paraview5/bin/vtkIOCoreCxxTests" "TestCompress" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing" "-T" "/usr/src/Paraview5/VTK/Testing/Temporary")
set_tests_properties(vtkIOCoreCxx-TestCompress PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
