# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/AMR
# Build directory: /usr/src/Paraview5/VTK/IO/AMR
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOAMR-HeaderTest "/usr/bin/python2" "/usr/src/ParaView-v5.0.0-source/VTK/Testing/Core/HeaderTesting.py" "/usr/src/ParaView-v5.0.0-source/VTK/IO/AMR" "VTKIOAMR_EXPORT")
set_tests_properties(vtkIOAMR-HeaderTest PROPERTIES  LABELS "")
