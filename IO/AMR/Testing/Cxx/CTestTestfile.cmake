# CMake generated Testfile for 
# Source directory: /usr/src/ParaView-v5.0.0-source/VTK/IO/AMR/Testing/Cxx
# Build directory: /usr/src/Paraview5/VTK/IO/AMR/Testing/Cxx
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(vtkIOAMRCxx-TestEnzoReader "/usr/src/Paraview5/bin/vtkIOAMRCxxTests" "TestEnzoReader" "-D" "/usr/src/Paraview5/ExternalData/VTK/Testing")
set_tests_properties(vtkIOAMRCxx-TestEnzoReader PROPERTIES  FAIL_REGULAR_EXPRESSION "(
|^)ERROR: " LABELS "")
